using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoudiApi.Data;
using BoudiApi.Dto;
using BoudiApi.Models;
using Microsoft.EntityFrameworkCore;

namespace BoudiApi.Repositories
{
  public class UsersRepository : IUsersRepository
  {
    private readonly ApplicationContext _context;

    public UsersRepository(ApplicationContext context)
    {
      _context = context;
    }
    
    public async Task<List<User>> GetAll(int maxRange)
    {
      return await _context.Users
        .Include(x => x.Posts)
        .Include(x => x.LikedPosts)
        .Include(x => x.SavedPosts)
        .Include(x => x.Followers)
        .Include(x => x.Following)
        .Include(x => x.Comments)
        .Take(maxRange).ToListAsync();
    }
    
    public async Task<User> GetSingle(Guid id)
    {
      var user = await _context.Users
        .Include(x => x.Posts)
        .ThenInclude(x => x.Category)
        .Include(x => x.LikedPosts)
        .ThenInclude(x => x.Category)
        .Include(x => x.SavedPosts)
        .ThenInclude(x => x.Category)
        .Include(x => x.Followers)
        .Include(x => x.Following)
        .Include(x => x.Comments)
      .FirstOrDefaultAsync(u => u.Id == id);
      return user;
    }
    public async Task<User> GetSingleByEmail(string email)
    {
      return await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
    }
    
    public async Task<List<User>> GetUserFollowers(Guid id)
    {
      var user = await _context.Users
        .Include(x => x.Followers)
        .Include(x => x.Following)
        .FirstOrDefaultAsync(u => u.Id == id);
      
      return user.Followers.ToList();
    }
    
    public async Task<User> CreateAsync(CrudUserDto userToCreate)
    {
      var model = new User();
      model.Id = Guid.NewGuid();
      model.FirstName = userToCreate.FirstName;
      model.LastName = userToCreate.LastName;
      model.Password = userToCreate.Password;
      model.Email = userToCreate.Email;
      model.Description = userToCreate.Description;
      model.Avatar = userToCreate.Avatar;

      _context.Users.Add(model);
      await _context.SaveChangesAsync();

      return model;
    }
    public async Task<User> UpdateAsync(Guid id, CrudUserDto userToEdit)
    {
      var model = await _context.Users.FindAsync(id);
      
      model.FirstName = userToEdit.FirstName;
      model.LastName = userToEdit.LastName;
      model.Password = userToEdit.Password;
      model.Email = userToEdit.Email;
      model.Description = userToEdit.Description;
      model.Avatar = userToEdit.Avatar;
      model.UpdatedAt = DateTime.Now;
      
      await _context.SaveChangesAsync();

      return await GetSingle(id);
    }
    
    public async Task<int> Delete(Guid id)
    {
      _context.Users.Remove(await _context.Users.FindAsync(id));
      
      return await _context.SaveChangesAsync();
    }
    
    public Task<bool> ExistByEmail(string email)
    {
      return _context.Users.AnyAsync(u => u.Email == email);
    } 
    
    public Task<bool> ExistById(Guid id)
    {
      return _context.Users.AnyAsync(u => u.Id == id);
    }
    
    public async Task<bool> IsPostAlreadyAdded(Guid userId, Guid postId, string context)
    {
      var user = await _context.Users
        .Include(x => x.Posts)
        .Include(x => x.SavedPosts)
        .Include(x => x.LikedPosts)
        .FirstOrDefaultAsync(x => x.Id == userId);

      return context switch
      {
        "saved" => user.SavedPosts.Any(x => x.Id == postId),
        "liked" => user.LikedPosts.Any(x => x.Id == postId),
        _ => user.Posts.Any(x => x.Id == postId)
      };
    }
    public async Task<bool> IsUserAlreadyFollowed(Guid userId, Guid userToFollow)
    {
      var user = await _context.Users
        .Include(x => x.Following)
        .FirstOrDefaultAsync(x => x.Id == userId);

      return user.Following.Any(x => x.Id == userToFollow);
    }
    
    public async Task<int> FollowUser(Guid id, Guid userId)
    {
      var userFollowing = await _context.Users
        .Include(x => x.Following)
        .FirstOrDefaultAsync(x => x.Id == id);
      
      var userFollowed = await _context.Users
        .Include(x => x.Followers)
        .FirstOrDefaultAsync(x => x.Id == userId);
      
      userFollowed.Followers.Add(userFollowing);
      userFollowing.Following.Add(userFollowed);
      
      return await _context.SaveChangesAsync();
    }
    public async Task<int> UnFollowUser(Guid id, Guid userId)
    {
      var userFollowing = await _context.Users
        .Include(x => x.Following)
        .FirstOrDefaultAsync(x => x.Id == id);
      
      var userFollowed = await _context.Users
        .Include(x => x.Followers)
        .FirstOrDefaultAsync(x => x.Id == userId);
      
      userFollowed.Followers.Remove(userFollowing);
      userFollowing.Following.Remove(userFollowed);
      
      return await _context.SaveChangesAsync();
    }
    
    public async Task<int> SavePost(Guid id, Guid postId)
    {
      var user = await _context.Users.FindAsync(id);
      var post = await _context.Posts
        .Include(x => x.Category)
        .FirstOrDefaultAsync(x => x.Id == postId);
      
      user.SavedPosts.Add(post);
      post.SavedBy.Add(user);
      
      return await _context.SaveChangesAsync();
    }
    
    public async Task<int> UnSavePost(Guid id, Guid postId)
    {
      
      var user = await _context.Users
        .Include(x => x.SavedPosts)
        .FirstOrDefaultAsync(x => x.Id == id);
      
      var post = await _context.Posts
        .Include(x => x.SavedBy)
        .FirstOrDefaultAsync(x => x.Id == postId);
      
      user.SavedPosts.Remove(post);
      post.SavedBy.Remove(user);
      
      return await _context.SaveChangesAsync();
    }

    
    public async Task<int> LikePost(Guid id, Guid postId)
    {
      var user = await _context.Users.FindAsync(id);
      var post = await _context.Posts
        .Include(x => x.Category)
        .FirstOrDefaultAsync(x => x.Id == postId);
      
      user.LikedPosts.Add(post);
      post.LikedBy.Add(user);
      
      return await _context.SaveChangesAsync();
    }
    
    public async Task<int> UnLikePost(Guid id, Guid postId)
    {
      
      var user = await _context.Users
        .Include(x => x.LikedPosts)
        .FirstOrDefaultAsync(x => x.Id == id);
      
      var post = await _context.Posts
        .Include(x => x.LikedBy)
        .FirstOrDefaultAsync(x => x.Id == postId);
      
      user.LikedPosts.Remove(post);
      post.LikedBy.Remove(user);
      
      return await _context.SaveChangesAsync();
    }

    public async Task<User> Login(LoginDto loginDto)
    {
      return await _context.Users
          .Include(x => x.Posts)
          .ThenInclude(x => x.Category)
          .Include(x => x.LikedPosts)
          .ThenInclude(x => x.Category)
          .Include(x => x.SavedPosts)
          .ThenInclude(x => x.Category)
          .Include(x => x.Followers)
          .Include(x => x.Following)
          .Include(x => x.Comments)
          .FirstOrDefaultAsync(u => u.Email == loginDto.Email && u.Password == loginDto.Password);
    }

  }
}