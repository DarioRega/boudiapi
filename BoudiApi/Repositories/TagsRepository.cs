using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoudiApi.Data;
using BoudiApi.Dto;
using BoudiApi.Models;
using Microsoft.EntityFrameworkCore;

namespace BoudiApi.Repositories
{
  public class TagsRepository : ITagsRepository
  {
    private readonly ApplicationContext _context;
    
    public TagsRepository(ApplicationContext context)
    {
      _context = context;
    }
    
    public async Task<List<Tag>> GetAll(string searchText, int maxRange)
    {
      return await _context.Tags
        .Where(x => x.Name.Contains(searchText))
        .Include(x => x.Posts)
        .Take(maxRange).ToListAsync();
    } 
    
    public async Task<Tag> GetSingle(Guid id)
    {
      return await _context.Tags
        .Include(x => x.Posts)
        .FirstOrDefaultAsync(u => u.Id == id);
    }
    
    public Task<bool> ExistById(Guid id)
    {
      return _context.Tags.AnyAsync(c => c.Id == id);
    }
    
    public Task<bool> ExistByName(string name)
    {
      return _context.Tags.AnyAsync(c => c.Name == name);
    }
    public Task<bool> ExistBySlug(string slug)
    {
      return _context.Tags.AnyAsync(c => c.Slug == slug);
    }

    public async Task<Tag> GetByName(string name)
    {
      return await _context.Tags.FirstOrDefaultAsync(u => u.Name == name);
    }

    public async Task<Tag> CreateAsync(CrudTagDto categoryToAdd, string generatedSlug)
    {
      var model = new Tag();
      model.Id = Guid.NewGuid();
      model.Name = categoryToAdd.Name;
      model.Slug = generatedSlug;

      _context.Tags.Add(model);
      await _context.SaveChangesAsync();

      return model;
    }
    
    public async Task<Tag> UpdateAsync(Guid id, CrudTagDto categoryToAdd, string generatedSlug)
    {
      var model = await _context.Tags.FindAsync(id);
      model.Name = categoryToAdd.Name;
      model.Slug = generatedSlug;
      model.UpdatedAt = DateTime.Now;

      await _context.SaveChangesAsync();

      return model;
    }
    
    public async Task<int> Delete(Guid id)
    {
      _context.Tags.Remove(await _context.Tags.FindAsync(id));
      
      return await _context.SaveChangesAsync();
    }
    public async Task <Tag> GetPostsBySingleTag(Guid tagId, int maxRange)
    {
      return await _context.Tags
      .Include("Posts")
      .Include("Posts.Category")
      .Include("Posts.User")
      .FirstOrDefaultAsync(x => x.Id == tagId);
    }


  }
}