using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Repositories
{
  public interface IPostsRepository
  {
    Task<List<Post>> GetAll(string searchText, int maxRange);
    Task<List<Post>> GetMostLikedPosts(int maxRange);
    Task<List<Post>> GetPostsRelatedToTags(List<Guid> tagIdList, int maxRange);
    Task<List<Post>> GetPostsByCategory(Guid categoryId, int maxRange);
    Task<Post> GetSingle(Guid id);
    Task<Post> GetBySlug(string slug);
    Task<Post> CreateAsync(CrudPostDto postToAdd, string slug);
    Task<Post> UpdateAsync(Guid id, CrudPostDto postToEdit, string slug);
    Task<int> Delete(Guid id);
    Task<bool> ExistBySlug(string slug);
    Task<bool> ExistById(Guid id);

  }
}