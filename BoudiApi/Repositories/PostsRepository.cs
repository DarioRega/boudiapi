using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoudiApi.Data;
using BoudiApi.Dto;
using BoudiApi.Models;
using Microsoft.EntityFrameworkCore;

namespace BoudiApi.Repositories
{
  public class PostsRepository : IPostsRepository
  {
    private readonly ApplicationContext _context;

    public PostsRepository(ApplicationContext context)
    {
      _context = context;
    }
    
    public async Task<List<Post>> GetAll(string searchText, int maxRange)
    {
      return await _context.Posts
        .Where(x => x.Title.Contains(searchText))
        .Include(x => x.Category)
        .Include(x => x.User)
        .Include(x => x.Comments)
        .Include(x => x.LikedBy)
        .Include(x => x.SavedBy)
        .Include(x => x.Tags)
        .Take(maxRange)
        .OrderByDescending(x => x.CreatedAt)
        .ToListAsync();
    }
    
    public async Task<List<Post>> GetMostLikedPosts(int maxRange)
    {
      return await _context.Posts
        .Include(x => x.Category)
        .Include(x => x.User)
        .Include(x => x.Comments)
        .Include(x => x.LikedBy)
        .Include(x => x.SavedBy)
        .Include(x => x.Tags)
        .OrderByDescending(x => x.LikedBy.Count)
        .Take(maxRange).ToListAsync();
    }
    
    public async Task<Post> GetSingle(Guid id)
    {
      var post = await _context.Posts
        .Include(x => x.Comments)
        .ThenInclude(x => x.User)
        .Include(x => x.LikedBy)
        .Include(x => x.User)
        .ThenInclude(x => x.Followers)
        .Include(x => x.SavedBy)
        .Include(x => x.Tags)
        .Include(x => x.Category)
        .FirstOrDefaultAsync(u => u.Id == id);
      
      return post;
    }    
    
    public async Task<Post> GetBySlug(string slug)
    {
      var post = await _context.Posts
        .Include(x => x.Comments)
        .Include(x => x.User)
        .Include(x => x.LikedBy)
        .Include(x => x.Tags)
        .Include(x => x.Category)
        .Include(x => x.SavedBy)
        .FirstOrDefaultAsync(u => u.Slug == slug);
      
      post.Comments.OrderByDescending(x => x.CreatedAt);

      return post;
    }
    
    public async Task<Post> CreateAsync(CrudPostDto postToAdd, string generatedSlug)
    {
      var user = await _context.Users.FindAsync(postToAdd.UserId);
      // WHY NULL ?
      var category = await _context.Categories.FindAsync(postToAdd.CategoryId);
      
      var tagListModels = new List<Tag>();
      if (postToAdd.TagsId != null)
      {
        var tagList = postToAdd.TagsId.ToList();
        tagListModels = await _context.Tags.Where(x => tagList.Contains(x.Id)).ToListAsync();
      }
      
      var model = new Post();
      model.Id = Guid.NewGuid();
      model.Title = postToAdd.Title;
      model.Slug = generatedSlug;
      model.Summary = postToAdd.Summary;
      model.Body = postToAdd.Body;
      model.ImageUrl = postToAdd.ImageUrl;
      model.Tags = tagListModels;
      model.UserId = postToAdd.UserId;
      model.User = user;
      model.CategoryId = postToAdd.CategoryId;
      model.Category = category;

      _context.Posts.Add(model);
      await _context.SaveChangesAsync();

      return model;
    }
    
    public async Task<Post> UpdateAsync(Guid id, CrudPostDto postToUpdate, string generatedSlug)
    {
      var model = await _context.Posts.Include(x => x.Tags).FirstAsync(x => x.Id == id);
      var user = await _context.Users.FindAsync(postToUpdate.UserId);
      var category = await _context.Categories.FindAsync(postToUpdate.CategoryId);
      
      var tagListModels = new List<Tag>();
      if (postToUpdate.TagsId != null)
      {
        var tagList = postToUpdate.TagsId.ToList();
        tagListModels = await _context.Tags.Where(x => tagList.Contains(x.Id)).ToListAsync();
      }

      model.Title = postToUpdate.Title;
      model.Slug = generatedSlug;
      model.Summary = postToUpdate.Summary;
      model.Body = postToUpdate.Body;
      model.ImageUrl = postToUpdate.ImageUrl;
      model.Tags = tagListModels;
      model.UserId = postToUpdate.UserId;
      model.User = user;
      model.CategoryId = postToUpdate.CategoryId;
      model.Category = category;

      await _context.SaveChangesAsync();

      return model;
    }
    
    public Task<bool> ExistBySlug(string slug)
    {
      return _context.Posts.AnyAsync(p => p.Slug == slug);
    }
    
    public Task<bool> ExistById(Guid id)
    {
      return _context.Posts.AnyAsync(c => c.Id == id);
    }
    
    public async Task<int> Delete(Guid id)
    {
      _context.Posts.Remove(await _context.Posts.FindAsync(id));
      
      return await _context.SaveChangesAsync();
    }
    
    public async Task<List<Post>> GetPostsByCategory(Guid categoryId, int maxRange)
    {
      return await _context.Posts
        .Where(x => x.CategoryId == categoryId)
        .Include(x => x.User)
        .Include(x => x.Category)
        .Include(x => x.Comments)
        .Include(x => x.LikedBy)
        .Include(x => x.SavedBy)
        .Take(maxRange).ToListAsync();
    }
    
    public async Task<List<Post>> GetPostsRelatedToTags(List<Guid> tagListId, int maxRange)
    {
        var relatedPosts = await _context.Posts
          .Include(x => x.User)
          .Include(x => x.Comments)
          .Include(x => x.Tags)
          .Include(x => x.Category)
          .Where(x => x.Tags.Any(t => tagListId.Contains(t.Id)))
          .Take(maxRange).ToListAsync();

        return relatedPosts;
    }
  }
}