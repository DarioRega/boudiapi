using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoudiApi.Data;
using BoudiApi.Dto;
using BoudiApi.Models;
using Microsoft.EntityFrameworkCore;

namespace BoudiApi.Repositories
{
  public class CommentsRepository : ICommentsRepository
  {
    private readonly ApplicationContext _context;
    
    public CommentsRepository(ApplicationContext context)
    {
      _context = context;
    }
    
    public async Task<List<Comment>> GetAll(int maxRange)
    {
      return await _context.Comments
        .Include(x => x.Post)
        .Take(maxRange).ToListAsync();
    } 
    
    public async Task<List<Comment>> GetRelatedComments(Guid postId, int maxRange)
    {
      return  await _context.Comments
        .Include(x => x.User)
        .Where(u => u.PostId == postId).ToListAsync();
    }   
    public async Task<Comment> GetSingle(Guid id)
    {
      return await _context.Comments
        .Include(x => x.Post)
        .FirstOrDefaultAsync(u => u.Id == id);
    }
    
    public Task<bool> ExistById(Guid id)
    {
      return _context.Comments.AnyAsync(c => c.Id == id);
    }

    public async Task<Comment> CreateAsync(CrudCommentDto commentToAdd)
    {
      var post = await _context.Posts.FindAsync(commentToAdd.PostId);
      var user = await _context.Users.FindAsync(commentToAdd.UserId);
      
      var model = new Comment();
      model.Id = Guid.NewGuid();
      model.Content = commentToAdd.Content ;
      model.PostId = post.Id ;
      model.Post = post ;
      model.UserId = user.Id;
      model.User = user;
    
      _context.Comments.Add(model);
      await _context.SaveChangesAsync();
    
      return model;
    }
    
    public async Task<Comment> UpdateAsync(Guid id, CrudCommentDto commentToEdit)
    {
      var model = await _context.Comments.FindAsync(id);
      var user = await _context.Users.FindAsync(commentToEdit.UserId);
      var post = await _context.Posts.FindAsync(commentToEdit.PostId);
      
      model.Content = commentToEdit.Content;
      model.UserId = user.Id;
      model.User = user;
      model.PostId = post.Id;
      model.Post = post;
      model.UpdatedAt = DateTime.Now;
    
      await _context.SaveChangesAsync();
    
      return model;
    }
    
    public async Task<int> Delete(Guid id)
    {
      _context.Comments.Remove(await _context.Comments.FindAsync(id));
      
      return await _context.SaveChangesAsync();
    }


  }
}