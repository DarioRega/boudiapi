using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoudiApi.Data;
using BoudiApi.Dto;
using BoudiApi.Models;
using Microsoft.EntityFrameworkCore;

namespace BoudiApi.Repositories
{
  public class CategoriesRepository : ICategoriesRepository
  {
    private readonly ApplicationContext _context;
    
    public CategoriesRepository(ApplicationContext context)
    {
      _context = context;
    }
    
    public async Task<List<Category>> GetAll(string searchText, int maxRange)
    {
      return await _context.Categories
        .Where(x => x.Name.Contains(searchText))
        .Include(x => x.Posts)
        .Take(maxRange).ToListAsync();
    } 
    
    public async Task<Category> GetSingle(Guid id)
    {
      return await _context.Categories
        .Include(x => x.Posts)
        .FirstOrDefaultAsync(u => u.Id == id);
    }
    
    public Task<bool> ExistById(Guid id)
    {
      return _context.Categories.AnyAsync(c => c.Id == id);
    }

    public Task<bool> ExistByName(string name)
    {
      return _context.Categories.AnyAsync(c => c.Name == name);
    }
    public Task<bool> ExistBySlug(string slug)
    {
      return _context.Categories.AnyAsync(c => c.Slug == slug);
    }

    public async Task<Category> GetByName(string name)
    {
      return await _context.Categories.FirstOrDefaultAsync(u => u.Name == name);
    }

    public async Task<Category> CreateAsync(CrudCategoryDto categoryToAdd, string generatedSlug)
    {
      var model = new Category();
      model.Id = Guid.NewGuid();
      model.Name = categoryToAdd.Name;
      model.Slug = generatedSlug;
      model.ThemeColor = categoryToAdd.ThemeColor;


      _context.Categories.Add(model);
      await _context.SaveChangesAsync();

      return model;
    }
    
    public async Task<Category> UpdateAsync(Guid id, CrudCategoryDto categoryToUpdate, string generatedSlug)
    {
      var model = await _context.Categories.FindAsync(id);
      model.Name = categoryToUpdate.Name;
      model.Slug = generatedSlug;
      model.ThemeColor = categoryToUpdate.ThemeColor;
      model.UpdatedAt = DateTime.Now;

      await _context.SaveChangesAsync();

      return model;
    }
    
    public async Task<int> Delete(Guid id)
    {
      _context.Categories.Remove(await _context.Categories.FindAsync(id));
      
      return await _context.SaveChangesAsync();
    }


  }
}