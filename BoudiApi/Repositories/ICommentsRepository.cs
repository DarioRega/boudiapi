using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Repositories
{
  public interface ICommentsRepository
  {
    Task<List<Comment>> GetAll(int maxRange);
    Task<List<Comment>> GetRelatedComments(Guid postId, int maxRange);
    Task<Comment> GetSingle(Guid id);
    Task<bool> ExistById(Guid id);
    Task<Comment> CreateAsync(CrudCommentDto commentToAdd);
    Task<Comment> UpdateAsync(Guid id, CrudCommentDto commentToEdit);
    Task<int> Delete(Guid id);

  }
}