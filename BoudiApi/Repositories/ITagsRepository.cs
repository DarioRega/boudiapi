using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Repositories
{
  public interface ITagsRepository
  {
    Task<List<Tag>> GetAll(string searchText, int maxRange);
    Task<Tag> GetSingle(Guid id);
    Task<Tag> GetPostsBySingleTag(Guid tagId, int maxRange);
    Task<bool> ExistById(Guid id);
    Task<bool> ExistByName(string name);
    Task<bool> ExistBySlug(string slug);
    Task<Tag> GetByName(string name);
    Task<Tag> CreateAsync(CrudTagDto categoryToAdd, string generatedSlug);
    Task<Tag> UpdateAsync(Guid id, CrudTagDto categoryToEdit, string generatedSlug);
    Task<int> Delete(Guid id);

  }
}