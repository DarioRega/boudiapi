using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Repositories
{
  public interface ICategoriesRepository
  {
    Task<List<Category>> GetAll(string searchText, int maxRange);
    Task<Category> GetSingle(Guid id);
    Task<bool> ExistById(Guid id);
    Task<bool> ExistByName(string name);
    Task<bool> ExistBySlug(string slug);
    Task<Category> GetByName(string name);
    Task<Category> CreateAsync(CrudCategoryDto categoryToAdd, string generatedSlug);
    Task<Category> UpdateAsync(Guid id, CrudCategoryDto categoryToEdit, string generatedSlug);
    Task<int> Delete(Guid id);

  }
}