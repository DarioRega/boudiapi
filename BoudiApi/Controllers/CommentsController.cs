using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BoudiApi.Dto;
using BoudiApi.Models;
using BoudiApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace BoudiApi.Controllers
{
  [ApiController]
  public class CommentsController : Controller
  {
      private readonly ICommentsService _commentsService;
      private readonly IMapper _mapper;

      public CommentsController(ICommentsService commentsService,  IMapper mapper)
      {
        _commentsService = commentsService;
        _mapper = mapper;
      }

        /// <summary>
        /// Get all comments.
        /// </summary>
        /// <param name="withRelatedPosts"></param>
        /// <param name="maxRange"></param>
        [HttpGet("comments")]
      public async Task<IActionResult> GetAll([FromQuery] bool withRelatedPosts = false, [FromQuery] int maxRange = 1000)
      {
          var comments = await _commentsService.GetAll(maxRange);
          
          if (withRelatedPosts)
            return Ok(_mapper.Map<List<Comment>, List<CommentDto>>(comments));

          return Ok(_mapper.Map<List<Comment>, List<CommentMiniatureDto>>(comments));
      }
      
         
      /// <summary>
      /// Get only comments related to a post.
      /// </summary>
      /// <param name="id"></param>
      /// <param name="isMiniature"></param>
      /// <param name="maxRange"></param>
      [HttpGet("comments/post/{id}")]
      public async Task<IActionResult> GetCommentsRelatedToPost([FromRoute] Guid id, [FromQuery] bool isMiniature = false, [FromQuery] int maxRange = 1000)
      {
        var comments = await _commentsService.GetRelatedComments(id, maxRange);
        if (isMiniature)
          return Ok(_mapper.Map<List<Comment>, List<CommentMiniatureDto>>(comments));
        return Ok(_mapper.Map<List<Comment>, List<CommentDto>>(comments));
      }

        /// <summary>
        /// Get single comment.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="withRelatedPosts"></param>
        [HttpGet("comments/{id}")]
      public async Task<IActionResult> GetSingle([FromRoute] Guid id, [FromQuery] bool withRelatedPosts = false)
      {
          var comment = await _commentsService.GetSingle(id);
          
          if (withRelatedPosts)
            return Ok(_mapper.Map<Comment,CommentDto>(comment));

          return Ok(_mapper.Map<Comment,CommentMiniatureDto>(comment));
      }   
      
      /// <summary>
      /// Create a single comment.
      /// </summary>
      /// <param name="commentToAdd"></param>
      [HttpPost("comments")]
      public async Task<IActionResult> Create([FromBody] CrudCommentDto commentToAdd)
      {
        var modelDb = await _commentsService.CreateAsync(commentToAdd);
      
        var modelDto = _mapper.Map<CommentMiniatureDto>(modelDb);
        return Ok(modelDto);
      }

        /// <summary>
        /// Update a single comment.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="commentToEdit"></param>
        [HttpPost("comments/{id}")]
      public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] CrudCommentDto commentToEdit)
      {
        var modelDb = await _commentsService.UpdateAsync(id, commentToEdit);
      
        var modelDto = _mapper.Map<CommentMiniatureDto>(modelDb);
        return Ok(modelDto);
      }
      
      /// <summary>
      /// Delete a single comment.
      /// </summary>
      /// <param name="id"></param>
      [HttpDelete("comments/{id}")]
      public async Task<IActionResult> Delete([FromRoute] Guid id)
      {
        await _commentsService.Delete(id);
        return NoContent();
      }

  }
}