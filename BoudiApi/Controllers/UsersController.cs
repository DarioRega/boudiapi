using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Threading.Tasks;
using AutoMapper;
using BoudiApi.Dto;
using BoudiApi.Models;
using BoudiApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace BoudiApi.Controllers
{
  [ApiController]
  public class UsersController : Controller
  {
    private readonly IUsersService _usersService;
    private readonly IMapper _mapper;

    public UsersController(IUsersService usersService,  IMapper mapper)
    {
      _usersService = usersService;
      _mapper = mapper;
    }


        /// <summary>
        /// Get all users.
        /// </summary>
        /// <param name="isMiniature"></param>
        /// <param name="maxRange"></param>
        [HttpGet("users")]
    public async Task<IActionResult> GetAll([FromQuery] bool isMiniature = false ,[FromQuery] int maxRange = 1000)
    {
        var users = await _usersService.GetAll(maxRange);
        if (isMiniature)
          return Ok(_mapper.Map<List<User>, List<UserMiniatureDto>>(users));
        return Ok(_mapper.Map<List<User>, List<UserDto>>(users));
    }   
     
     /// <summary>
     /// Get single user.
     /// </summary>
     /// <param name="id"></param>
     /// <param name="isMiniature"></param>
     /// <param name="withPosts"></param>
     [HttpGet("users/{id}")]
     public async Task<IActionResult> GetSingle([FromRoute] Guid id, [FromQuery] bool isMiniature = false, bool withPosts = false)
     {
         var user = await _usersService.GetSingle(id);

         if (user == null)
           return NotFound();

         if (isMiniature)
         {
             if (withPosts)
                 return Ok(_mapper.Map<UserMiniatureDtoWithPosts>(user));
           return Ok(_mapper.Map<UserMiniatureDto>(user));
         }

         return Ok(_mapper.Map<UserDto>(user));
     }
     
     /// <summary>
     /// Create a single user.
     /// </summary>
     /// <param name="userToCreate"></param>
     [HttpPost("users")]
     public async Task<IActionResult> Create([FromBody] CrudUserDto userToCreate)
     {
       var modelDb = await _usersService.CreateAsync(userToCreate);

        var modelDto = _mapper.Map<UserDto>(modelDb);
        return Created($"users/{modelDto.Id}", modelDto);
     }

        /// <summary>
        /// Update a single user.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userToEdit"></param>
        [HttpPost("users/{id}")]
     public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] CrudUserDto userToEdit)
     {
         var modelDb = await _usersService.UpdateAsync(id, userToEdit);

         var modelDto = _mapper.Map<UserDto>(modelDb);
         return Ok(modelDto);
     }
     
     /// <summary>
     /// Delete a single user.
     /// </summary>
     /// <param name="id"></param>
     [HttpDelete("users/{id}")]
     public async Task<IActionResult> Delete(Guid id)
     {
         await _usersService.Delete(id);
         return NoContent();
     }
     
    /// <summary>
    /// Get user followers.
    /// </summary>
    /// <param name="id"></param>
    [HttpGet("users/{id}/followers")]
    public async Task<IActionResult> GetUserFollowers([FromRoute]  Guid id)
    {
        var followers = await _usersService.GetUserFollowers(id);
        return Ok(_mapper.Map<List<User>, List<UserMiniatureDto>>(followers));
    }
    
    /// <summary>
    /// Follow an user.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="userId"></param>
    [HttpPost("users/{id}/follow/{userId}")]
    public async Task<IActionResult> FollowUser([FromRoute]  Guid id, [FromRoute] Guid userId)
    {
        return Ok(await _usersService.FollowUser(id, userId));
    }
    
    /// <summary>
    /// Unfollow an user.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="userId"></param>
    [HttpDelete("users/{id}/unfollow/{userId}")]
    public async Task<IActionResult> UnFollowUser([FromRoute]  Guid id, [FromRoute] Guid userId)
    {
        return Ok(await _usersService.UnFollowUser(id, userId));
    }
   
    /// <summary>
    /// Save a post.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="postId"></param>
    [HttpPost("users/{id}/save-post/{postId}")]
    public async Task<IActionResult> SavePost([FromRoute] Guid id, Guid postId)
    {
        await _usersService.SavePost(id, postId);
        return Ok();
    }
    /// <summary>
    /// Unsave a post.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="postId"></param>
    [HttpDelete("users/{id}/unsave-post/{postId}")]
    public async Task<IActionResult> UnSavePost([FromRoute] Guid id, Guid postId)
    {
        await _usersService.UnSavePost(id, postId);
        return Ok();
    }
    
    /// <summary>
    /// Like a post.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="postId"></param>
    [HttpPost("users/{id}/like-post/{postId}")]
    public async Task<IActionResult> LikePost([FromRoute] Guid id, Guid postId)
        {
        await _usersService.LikePost(id, postId);
        return Ok();
    }
    
    /// <summary>
    /// Unlike a post.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="postId"></param>
    [HttpDelete("users/{id}/unlike-post/{postId}")]
    public async Task<IActionResult> UnLikePost([FromRoute] Guid id, Guid postId)
    {
        await _usersService.UnLikePost(id, postId);
        return Ok();
    }
    
       
    /// <summary>
    /// Login
    /// </summary>
    /// <param name="loginDto"></param>
    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] LoginDto loginDto)
    {
        var user = await _usersService.Login(loginDto);
        return Ok(_mapper.Map<UserDto>(user));
    }   
  }
}