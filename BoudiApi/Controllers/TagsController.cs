using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BoudiApi.Dto;
using BoudiApi.Models;
using BoudiApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace BoudiApi.Controllers
{
  [ApiController]
  public class TagsController : Controller
  {
      private readonly ITagsService _tagsService;
      private readonly IMapper _mapper;

      public TagsController(ITagsService tagsService,  IMapper mapper)
      {
        _tagsService = tagsService;
        _mapper = mapper;
      }
      
      /// <summary>
      /// Get all tags.
      /// </summary>
      /// <param name="searchText"></param>
      /// <param name="withRelatedPosts"></param>
      /// <param name="maxRange"></param>
      [HttpGet("tags")]
      public async Task<IActionResult> GetAll([FromQuery] string searchText = "", [FromQuery] bool withRelatedPosts = false, [FromQuery] int maxRange = 1000)
      {
          var tags = await _tagsService.GetAll(searchText, maxRange);
          
          if (withRelatedPosts)
            return Ok(_mapper.Map<List<Tag>, List<TagDto>>(tags));

          return Ok(_mapper.Map<List<Tag>, List<TagMiniatureWithCountDto>>(tags));
      }

        /// <summary>
        /// Get single tag.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="withRelatedPosts"></param>
        [HttpGet("tags/{id}")]
      public async Task<IActionResult> GetSingle([FromRoute] Guid id, [FromQuery] bool withRelatedPosts = false)
      {
          var tag = await _tagsService.GetSingle(id);
          
          if (withRelatedPosts)
            return Ok(_mapper.Map<Tag,TagDto>(tag));

          return Ok(_mapper.Map<Tag,TagMiniatureWithCountDto>(tag));
      }   
      
      /// <summary>
      /// Create a single tag.
      /// </summary>
      /// <param name="tagToAdd"></param>
      [HttpPost("tags")]
      public async Task<IActionResult> Create([FromBody] CrudTagDto tagToAdd)
      {
        var modelDb = await _tagsService.CreateAsync(tagToAdd);

        var modelDto = _mapper.Map<TagMiniatureDto>(modelDb);
        return Created($"tags/{modelDto.Id}", modelDto);
      }

        /// <summary>
        /// Update a single tag.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tagToEdit"></param>
        [HttpPost("tags/{id}")]
      public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] CrudTagDto tagToEdit)
      {
        var modelDb = await _tagsService.UpdateAsync(id, tagToEdit);

        var modelDto = _mapper.Map<TagMiniatureDto>(modelDb);
        return Ok(modelDto);
      }
      
      /// <summary>
      /// Delete a single tag.
      /// </summary>
      /// <param name="id"></param>
      [HttpDelete("tags/{id}")]
      public async Task<IActionResult> Delete([FromRoute] Guid id)
      {
        await _tagsService.Delete(id);
        return NoContent();
      }

  }
}