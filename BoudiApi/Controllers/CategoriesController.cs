using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BoudiApi.Dto;
using BoudiApi.Models;
using BoudiApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace BoudiApi.Controllers
{
  [ApiController]
  public class CategoriesController : Controller
  {
      private readonly ICategoriesService _categoriesService;
      private readonly IMapper _mapper;

      public CategoriesController(ICategoriesService categoriesService,  IMapper mapper)
      {
        _categoriesService = categoriesService;
        _mapper = mapper;
      }

        /// <summary>
        /// Get all categories.
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="withRelatedPosts"></param>
        /// <param name="maxRange"></param>
        [HttpGet("categories")]
      public async Task<IActionResult> GetAll([FromQuery] string searchText = "" , [FromQuery] bool withRelatedPosts = false, [FromQuery] int maxRange = 1000)
      {
          var categories = await _categoriesService.GetAll(searchText, maxRange);
          
          if (withRelatedPosts)
            return Ok(_mapper.Map<List<Category>, List<CategoryDto>>(categories));

          return Ok(_mapper.Map<List<Category>, List<CategoryMiniatureDto>>(categories));
      }

        /// <summary>
        /// Get single category.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="withRelatedPosts"></param>
        [HttpGet("categories/{id}")]
      public async Task<IActionResult> GetSingle([FromRoute] Guid id, [FromQuery] bool withRelatedPosts = false)
      {
          var category = await _categoriesService.GetSingle(id);
          
          if (withRelatedPosts)
            return Ok(_mapper.Map<Category,CategoryDto>(category));

          return Ok(_mapper.Map<Category,CategoryMiniatureDto>(category));
      }   
      
      /// <summary>
      /// Create a single category.
      /// </summary>
      /// <param name="categoryToAdd"></param>
      [HttpPost("categories")]
      public async Task<IActionResult> Create([FromBody] CrudCategoryDto categoryToAdd)
      {
        var modelDb = await _categoriesService.CreateAsync(categoryToAdd);

        var modelDto = _mapper.Map<CategoryMiniatureDto>(modelDb);
        return Created($"categories/{modelDto.Id}", modelDto);
      }


        /// <summary>
        /// Update a single category.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="categoryToEdit"></param>
        [HttpPost("categories/{id}")]
      public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] CrudCategoryDto categoryToEdit)
      {
        var modelDb = await _categoriesService.UpdateAsync(id, categoryToEdit);

        var modelDto = _mapper.Map<CategoryMiniatureDto>(modelDb);
        return Ok(modelDto);
      }
      
      /// <summary>
      /// Delete a single category.
      /// </summary>
      /// <param name="id"></param>
      [HttpDelete("categories/{id}")]
      public async Task<IActionResult> Delete([FromRoute] Guid id)
      {
        await _categoriesService.Delete(id);
        return NoContent();
      }

  }
}