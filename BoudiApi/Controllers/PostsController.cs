using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BoudiApi.Dto;
using BoudiApi.Models;
using BoudiApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace BoudiApi.Controllers
{
  [ApiController]
  public class PostsController : Controller
  {
    
    private readonly IPostsService _postsService;
    private readonly ITagsService _tagsService;
    private readonly IMapper _mapper;

    public PostsController(IPostsService postsService,ITagsService tagsService,  IMapper mapper)
    {
      _postsService = postsService;
      _tagsService = tagsService;
      _mapper = mapper;
    }
    
    /// <summary>
    /// Get all posts.
    /// </summary>
    /// <param name="searchText"></param>
    /// <param name="isMiniature"></param>
    /// <param name="maxRange"></param>
    [HttpGet("posts")]
    public async Task<IActionResult> GetAll([FromQuery] string searchText = "", [FromQuery] bool isMiniature = false ,[FromQuery] int maxRange = 1000)
    {
      var posts = await _postsService.GetAll(searchText, maxRange);
      if (isMiniature)
        return Ok(_mapper.Map<List<Post>, List<PostMiniatureDto>>(posts));
      return Ok(_mapper.Map<List<Post>, List<PostDto>>(posts));
    }
    
    /// <summary>
    /// Get most liked posts.
    /// </summary>
    /// <param name="isMiniature"></param>
    /// <param name="maxRange"></param>
    [HttpGet("posts/most-liked")]
    public async Task<IActionResult> GetMostLikedPosts([FromQuery] bool isMiniature, [FromQuery]int maxRange = 1000)
    {
      var posts = await _postsService.GetMostLikedPosts(maxRange);
      if (isMiniature)
        return Ok(_mapper.Map<List<Post>, List<PostMiniatureDto>>(posts));
      return Ok(_mapper.Map<List<Post>, List<PostDto>>(posts));
    }
   /// <summary>
    /// Get posts matching taglist id's.
    /// </summary>
    /// <param name="tagIdList"></param>
    /// <param name="isMiniature"></param>
    /// <param name="maxRange"></param>
    [HttpPost("posts/related-tags")]
    public async Task<IActionResult> GetPostsRelatedToTags([FromBody]List<Guid> tagIdList, [FromQuery] bool isMiniature, [FromQuery]int maxRange = 1000)
    {
      var posts = await _postsService.GetPostsRelatedToTags(tagIdList, maxRange);
      if (isMiniature)
        return Ok(_mapper.Map<List<Post>, List<PostMiniatureDto>>(posts));
      return Ok(_mapper.Map<List<Post>, List<PostDto>>(posts));
    }

        /// <summary>
        /// Get single post.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isMiniature"></param>
        [HttpGet("posts/{id}")]
    public async Task<IActionResult> GetSingle([FromRoute] Guid id, [FromQuery] bool isMiniature)
    {
      var user = await _postsService.GetSingle(id);

      if (user == null)
        return NotFound();

      if (isMiniature)
        return Ok(_mapper.Map<PostMiniatureDto>(user));

      return Ok(_mapper.Map<PostDto>(user));
    }
    
    /// <summary>
    /// Create a single post.
    /// </summary>
    /// <param name="postToCreate"></param>
    [HttpPost("posts")]
    public async Task<IActionResult> Create([FromBody] CrudPostDto postToCreate)
    {
      var modelDb = await _postsService.CreateAsync(postToCreate);

      var modelDto = _mapper.Map<PostDto>(modelDb);
      return Ok(modelDto);
    }
    /// <summary>
    /// Update a single post.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="postToUpdate"></param>
    [HttpPost("posts/{id}")]
    public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] CrudPostDto postToUpdate)
    {
      var modelDb = await _postsService.UpdateAsync(id, postToUpdate);

      var modelDto = _mapper.Map<PostDto>(modelDb);
      return Ok(modelDto);
    }
    
    /// <summary>
    /// Delete a single post.
    /// </summary>
    /// <param name="id"></param>
    [HttpDelete("posts/{id}")]
    public async Task<IActionResult> Delete([FromRoute] Guid id)
    {
      await _postsService.Delete(id);
      return NoContent();
    }
    
    /// <summary>
    /// Get posts by category.
    /// </summary>
    /// <param name="categoryId"></param>
    /// <param name="isMiniature"></param>
    /// <param name="maxRange"></param>
    [HttpGet("posts/category/{categoryId}")]
    public async Task<IActionResult> GetByCategory([FromRoute] Guid categoryId, [FromQuery] bool isMiniature = false ,[FromQuery] int maxRange = 1000)
    {
      var posts = await _postsService.GetPostsByCategory(categoryId, maxRange);
      if (isMiniature)
        return Ok(_mapper.Map<List<Post>, List<PostMiniatureDto>>(posts));
      return Ok(_mapper.Map<List<Post>, List<PostDto>>(posts));
    }
    
    /// <summary>
    /// Get posts by category.
    /// </summary>
    /// <param name="tagId"></param>
    /// <param name="maxRange"></param>
    [HttpGet("posts/tags/{tagId}")]
    public async Task<IActionResult> GetBySingleTag([FromRoute] Guid tagId, [FromQuery] int maxRange = 1000)
    {
      var posts = await _tagsService.GetPostsBySingleTag(tagId, maxRange);
      return Ok(_mapper.Map<RelatedPostTagDto>(posts));
    }
  }
}