using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Security.Authentication;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BoudiApi.Middlewares
{
  public class ExceptionMiddleware
  {
    public ExceptionMiddleware()
    {
    }
 
    public async Task Invoke(HttpContext context)
    {
      var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
      if (contextFeature != null && contextFeature.Error != null)
      {
        context.Response.StatusCode = (int)GetErrorCode(contextFeature.Error);
        context.Response.ContentType = "application/json";
 
        await context.Response.WriteAsync(JsonConvert.SerializeObject(new ProblemDetails()
        {
          Status = context.Response.StatusCode,
          Title = contextFeature.Error.Message
        }));
      }
    }
 
    private static HttpStatusCode GetErrorCode(Exception e)
    {
      switch (e)
      {
        case InvalidCredentialException _:
          return HttpStatusCode.Unauthorized;
        case ValidationException _:
          return HttpStatusCode.BadRequest;
        case FormatException _:
          return HttpStatusCode.BadRequest;
        case ArgumentException _:
          return HttpStatusCode.BadRequest;        
        case InvalidDataException _:
          return HttpStatusCode.BadRequest;
        case NullReferenceException _:
          return HttpStatusCode.NotFound;
        case AuthenticationException _:
          return HttpStatusCode.Forbidden;
        case NotImplementedException _:
          return HttpStatusCode.NotImplemented;
        default:
          return HttpStatusCode.InternalServerError;
      }
    }
  }
}