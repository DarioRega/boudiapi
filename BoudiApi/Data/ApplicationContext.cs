using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BoudiApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;
using TimeZoneConverter;

namespace BoudiApi.Data
{
  public class ApplicationContext : DbContext
  {
    public DbSet<User> Users { get; set; }
    public DbSet<Post> Posts { get; set; }
    public DbSet<Comment> Comments { get; set; }
    public DbSet<Tag> Tags { get; set; }
    public DbSet<Category> Categories { get; set; }

    public ApplicationContext(DbContextOptions<ApplicationContext> options)
      : base(options) 
    {
        
    }
    
    public override int SaveChanges()
    {
      AddTimestamps();
      return base.SaveChanges();
    }

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
      AddTimestamps();
      return await base.SaveChangesAsync(cancellationToken);
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

      modelBuilder.Entity<Comment>(entity =>
      {
        entity.Property(c => c.Id).HasConversion<string>();
        
        entity.HasOne<Post>(p => p.Post)
        .WithMany(p => p.Comments)
        .OnDelete(DeleteBehavior.Cascade);

        entity.HasOne<User>(u => u.User)
          .WithMany(u => u.Comments);
      });
      
      modelBuilder.Entity<User>(entity =>
      {
        entity.Property(u => u.Id).HasConversion<string>();

        entity.HasMany(u => u.Posts)
          .WithOne(p => p.User);
        
        entity.HasMany(u => u.LikedPosts)
          .WithMany(p => p.LikedBy);
        
        entity.HasMany(u => u.SavedPosts)
          .WithMany(p => p.SavedBy);

        entity.HasMany(x => x.Followers)
          .WithMany(x => x.Following);
      });    
      
      modelBuilder.Entity<Post>(entity =>
      {
        entity.Property(p => p.Id).HasConversion<string>();
       
        entity.HasMany<Comment>(p => p.Comments)
          .WithOne(p => p.Post)
          .OnDelete(DeleteBehavior.Cascade);
        
        entity.HasMany(p => p.Tags)
          .WithMany(p => p.Posts);
      });
      
      modelBuilder.Entity<Category>().Property(c => c.Id).HasConversion<string>();
      modelBuilder.Entity<Tag>().Property(t=> t.Id).HasConversion<string>();

      
    }
    
    private void AddTimestamps()
    {
      var entities = ChangeTracker.Entries()
        .Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));
    
      foreach (var entity in entities)
      {
        var timeZones = TimeZoneInfo.GetSystemTimeZones();
        var Utcnow = DateTime.UtcNow; // current datetime
        var localNow = GetLocalTime(Utcnow, "Europe/Zurich");
        
        if (entity.State == EntityState.Added)
        {
          ((BaseEntity)entity.Entity).CreatedAt = localNow;
        }

        ((BaseEntity) entity.Entity).UpdatedAt = localNow;
      }
      
    }
    public static DateTime GetLocalTime(DateTime utc, string timeZone)
    {

      bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
      if (isWindows) {
        timeZone = TZConvert.IanaToWindows(timeZone);
      }
    
      var local =
        TimeZoneInfo.ConvertTimeFromUtc(utc,
          TimeZoneInfo.FindSystemTimeZoneById(timeZone));
    
      return local;
    }

  }
}