using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Authentication;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Helpers;
using BoudiApi.Models;
using BoudiApi.Repositories;

namespace BoudiApi.Services
{
  public class UsersService : IUsersService
  {
    private readonly IUsersRepository _usersRepository;
    private readonly IPostsRepository _postsRepository;
    private readonly IHelper _helper;

    public UsersService(IUsersRepository usersRepository, IPostsRepository postsRepository, IHelper helper)
    {
      _usersRepository = usersRepository;
      _postsRepository = postsRepository;
      _helper = helper;
    }
    
    public async Task<List<User>> GetAll(int maxRange = 1000)
    {
      return  await _usersRepository.GetAll(maxRange);
    }
    
    public async Task<User> GetSingle(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if(! await _usersRepository.ExistById(id))
        throw new NullReferenceException("User doesn't exist");
        
      return await _usersRepository.GetSingle(id);
    }
    
    public async Task<List<User>> GetUserFollowers(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if(! await _usersRepository.ExistById(id))
        throw new NullReferenceException("User doesn't exist");
      
      return await _usersRepository.GetUserFollowers(id);
    }

    public async Task<User> CreateAsync(CrudUserDto userToCreate)
    {
      if(await _usersRepository.ExistByEmail(userToCreate.Email))
        throw new InvalidDataException($"Email already exist");
      
      var modelDb = await _usersRepository.CreateAsync(userToCreate);
      return modelDb;
    }
    
    // TODO ADD TEST
    public async Task<User> UpdateAsync(Guid id, CrudUserDto userToEdit)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if(! await _usersRepository.ExistById(id))
        throw new NullReferenceException("User doesn't exist");

      if (await _usersRepository.ExistByEmail(userToEdit.Email))
      {
        var userWithEmail = await _usersRepository.GetSingleByEmail(userToEdit.Email);
        if(userWithEmail.Id != id)
          throw new InvalidDataException($"Email already exist");
      }
      
      var modelDb =await _usersRepository.UpdateAsync(id, userToEdit);
      return modelDb;
    }
    
    public async Task<int> Delete(Guid userId)
    {
      if(! await _usersRepository.ExistById(userId))
        throw new NullReferenceException("User doesn't exist");
      
      return await _usersRepository.Delete(userId);
    }
    
    public async Task<int> FollowUser(Guid id, Guid userId)
    {
      if(_helper.IsNullOrEmpty(id) || _helper.IsNullOrEmpty(userId))
        throw new  ArgumentException("Invalid id");
      
      if(!await _usersRepository.ExistById(id) || !await _usersRepository.ExistById(userId))
        throw new NullReferenceException("User must exist to be perform this action");
        
      if(await _usersRepository.IsUserAlreadyFollowed(id, userId))
        throw new ArgumentException("User is already following this user.");
      
      return await _usersRepository.FollowUser(id, userId);
    }
    
    public async Task<int> UnFollowUser(Guid id, Guid userId)
    {
      if(_helper.IsNullOrEmpty(id) || _helper.IsNullOrEmpty(userId))
        throw new  ArgumentException("Invalid id");

      if(!await _usersRepository.ExistById(id) || !await _usersRepository.ExistById(userId))
        throw new NullReferenceException("User must exist to be perform this action");
      
      if(!await _usersRepository.IsUserAlreadyFollowed(id, userId))
        throw new ArgumentException("User is not following this user.");
      
      return await _usersRepository.UnFollowUser(id, userId);
    }
    
    
    public async Task<int> SavePost(Guid id, Guid postId)
    {
      if(_helper.IsNullOrEmpty(id) || _helper.IsNullOrEmpty(postId))
        throw new  ArgumentException("Invalid id");

      if(!await _usersRepository.ExistById(id) || !await _postsRepository.ExistById(postId))
        throw new NullReferenceException("User or Post must exist to be perform this action");
      
      if(await _usersRepository.IsPostAlreadyAdded(id, postId, "saved"))
        throw new ArgumentException("Post already saved");
      
      return await _usersRepository.SavePost(id, postId);
    }
    
    public async Task<int> UnSavePost(Guid id, Guid postId)
    {
      if(_helper.IsNullOrEmpty(id) || _helper.IsNullOrEmpty(postId))
        throw new  ArgumentException("Invalid id");
      
      if(!await _usersRepository.ExistById(id) || !await _postsRepository.ExistById(postId))
        throw new NullReferenceException("User or Post must exist to be perform this action");
      
      if(!await _usersRepository.IsPostAlreadyAdded(id, postId, "saved"))
        throw new ArgumentException("Post not in your saved list");
      
      return await _usersRepository.UnSavePost(id, postId);
    }
    
    public async Task<int> LikePost(Guid id, Guid postId)
    {
      if(_helper.IsNullOrEmpty(id) || _helper.IsNullOrEmpty(postId))
        throw new  ArgumentException("Invalid id");

        if(!await _usersRepository.ExistById(id) || !await _postsRepository.ExistById(postId))
        throw new NullReferenceException("User or Post must exist to be perform this action");
      
      if(await _usersRepository.IsPostAlreadyAdded(id, postId, "liked"))
        throw new ArgumentException("Post already liked");
      
      return await _usersRepository.LikePost(id, postId);
    }
    
    public async Task<int> UnLikePost(Guid id, Guid postId)
    {
      if(_helper.IsNullOrEmpty(id) || _helper.IsNullOrEmpty(postId))
        throw new  ArgumentException("Invalid id");

      if(!await _usersRepository.ExistById(id) || !await _postsRepository.ExistById(postId))
        throw new NullReferenceException("User or Post must exist to be perform this action");
      
      if(!await _usersRepository.IsPostAlreadyAdded(id, postId, "liked"))
        throw new ArgumentException("Post not in your liked list");
      
      return await _usersRepository.UnLikePost(id, postId);
    }

    public async Task<User> Login(LoginDto loginDto)
    {
      if (loginDto.Email == null || loginDto.Password == null)
        throw new ArgumentException("You must provide an email and a password to login.");
      
      var user = await _usersRepository.Login(loginDto);
      if(user == null)
        throw new InvalidCredentialException("Invalid credentials.");
      
      return user;
    }

  }
}