using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Services
{
  public interface ICategoriesService
  {
    Task<List<Category>> GetAll(string searchText, int maxRange);
    Task<Category> GetSingle(Guid id);
    Task<Category> CreateAsync(CrudCategoryDto categoryToAdd);
    Task<Category> UpdateAsync(Guid id, CrudCategoryDto categoryToUpdate);
    Task<int> Delete(Guid id);

  }
}