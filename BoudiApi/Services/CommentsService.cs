using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Helpers;
using BoudiApi.Models;
using BoudiApi.Repositories;

namespace BoudiApi.Services
{
  public class CommentsService : ICommentsService
  {
    private readonly ICommentsRepository _commentsRepository;
    private readonly IPostsRepository _postsRepository;
    private readonly IUsersRepository _usersRepository;
    private readonly IHelper _helper;

    public CommentsService(ICommentsRepository commentsRepository, IPostsRepository postsRepository,IUsersRepository usersRepository, IHelper helper)
    {
      _commentsRepository = commentsRepository;
      _usersRepository = usersRepository;
      _postsRepository = postsRepository;
      _helper = helper;
    }
    
    public async Task<List<Comment>> GetAll(int maxRange)
    {
      return await _commentsRepository.GetAll(maxRange);
    }
    
    public async Task<List<Comment>> GetRelatedComments(Guid postId, int maxRange)
    {
      return  await _commentsRepository.GetRelatedComments(postId, maxRange);
    }    
    
    public async Task<Comment> GetSingle(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
      {
        throw new  ArgumentException("Invalid id");
      }
      
      if(! await _commentsRepository.ExistById(id))
        throw new ArgumentException("Comment doesn't exist");
      
      return await _commentsRepository.GetSingle(id);
    }
    
    public async Task<Comment> CreateAsync(CrudCommentDto commentToAdd)
    {
      if (!await _postsRepository.ExistById(commentToAdd.PostId) || !await _usersRepository.ExistById(commentToAdd.UserId))
        throw new NullReferenceException($"User or post doesn't exist.");
      
      var modelDb =await _commentsRepository.CreateAsync(commentToAdd);
      return modelDb;
    }
    
    public async Task<Comment> UpdateAsync(Guid id, CrudCommentDto commentToEdit)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if (!await _commentsRepository.ExistById(id))
        throw new  NullReferenceException("Comment doesn't exist");
      
      if (!await _postsRepository.ExistById(commentToEdit.PostId) || !await _usersRepository.ExistById(commentToEdit.UserId))
        throw new NullReferenceException($"User or post doesn't exist.");
      
      
      var modelDb = await _commentsRepository.UpdateAsync(id, commentToEdit);
      
      return modelDb;
    }
    
    public async Task<int> Delete(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if (!await _commentsRepository.ExistById(id))
        throw new  NullReferenceException("Comment doesn't exist");

      return await _commentsRepository.Delete(id);
    }

  }
}