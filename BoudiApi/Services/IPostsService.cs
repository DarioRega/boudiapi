using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Services
{
  public interface IPostsService
  {
    Task<List<Post>> GetAll(string searchText, int maxRange);
    Task<List<Post>> GetMostLikedPosts(int maxRange);
    Task<List<Post>> GetPostsRelatedToTags(List<Guid> tagIdList, int maxRange);
    Task<List<Post>> GetPostsByCategory(Guid categoryId, int maxRange);
    Task<Post> GetSingle(Guid id);
    Task<Post> CreateAsync(CrudPostDto postToAdd);
    Task<Post> UpdateAsync(Guid id, CrudPostDto postToEdit);
    Task<int> Delete(Guid id);
  }
}