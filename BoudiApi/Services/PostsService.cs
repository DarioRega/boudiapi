using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Helpers;
using BoudiApi.Models;
using BoudiApi.Repositories;
using Microsoft.AspNetCore.Http;

namespace BoudiApi.Services
{
  public class PostsService : IPostsService
  {
    private readonly IPostsRepository _postsRepository;
    private readonly ICategoriesRepository _categoriesRepository;
    private readonly IUsersRepository _usersRepository;
    private readonly IHelper _helper;

    public PostsService(IPostsRepository postsRepository, ICategoriesRepository categoriesRepository, IUsersRepository usersRepository, IHelper helper)
    {
      _postsRepository = postsRepository;
      _categoriesRepository = categoriesRepository;
      _usersRepository = usersRepository;
      _helper = helper;
    }
    
    public async Task<List<Post>> GetAll(string searchText, int maxRange)
    {
      return  await _postsRepository.GetAll(searchText, maxRange);
    }

    public async Task<List<Post>> GetMostLikedPosts(int maxRange)
    {
      return  await _postsRepository.GetMostLikedPosts(maxRange);
    }   
    public async Task<List<Post>> GetPostsRelatedToTags(List<Guid> tagIdList, int maxRange)
    {
      if (tagIdList == null || tagIdList.Count < 1)
        throw new ArgumentException("Cannot fetch related post to tags if the list is empty");
      
      return  await _postsRepository.GetPostsRelatedToTags(tagIdList, maxRange);
    }
    
    public async Task<Post> GetSingle(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if(! await _postsRepository.ExistById(id))
        throw new NullReferenceException("Post doesn't exist");
      
      return await _postsRepository.GetSingle(id);
    }
    
    public async Task<Post> CreateAsync(CrudPostDto postToCreate)
    {
      if(_helper.IsNullOrEmpty(postToCreate.UserId) || _helper.IsNullOrEmpty(postToCreate.CategoryId))
        throw new  ArgumentException("Invalid id");
      
      if(!await _usersRepository.ExistById(postToCreate.UserId))
        throw new NullReferenceException($"User with this id doesn't exist.");
      
      if(!await _categoriesRepository.ExistById(postToCreate.CategoryId))
        throw new NullReferenceException($"Category with this id doesn't exist.");
      
      var slug = _helper.GenerateSlug(postToCreate.Title);

      if(await _postsRepository.ExistBySlug(slug))
        throw new InvalidDataException($"Post with same slug already exist.");

      
      var modelDb = await _postsRepository.CreateAsync(postToCreate, slug);
      return modelDb;
    }
    
    public async Task<Post> UpdateAsync(Guid id, CrudPostDto postToEdit)
    {
      if(_helper.IsNullOrEmpty(id) || _helper.IsNullOrEmpty(postToEdit.UserId))
        throw new  ArgumentException("Invalid id");
      
      if (!await _postsRepository.ExistById(id) || !await _usersRepository.ExistById(postToEdit.UserId))
        throw new  NullReferenceException("Post doesn't exist");

      var slug = _helper.GenerateSlug(postToEdit.Title);

      if (await _postsRepository.ExistBySlug(slug))
      {
        var postWithSameSlug = await _postsRepository.GetBySlug(slug);
        
        if(postWithSameSlug.Id != id)
          throw new InvalidDataException($"Post with same slug already exist");
      }
      
      var modelDb = await _postsRepository.UpdateAsync(id, postToEdit, slug);
      
      return modelDb;
    }
    
    public async Task<int> Delete(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if (!await _postsRepository.ExistById(id))
        throw new NullReferenceException("Post doesn't exist");

      return await _postsRepository.Delete(id);
    }
    
    public async Task<List<Post>> GetPostsByCategory(Guid categoryId, int maxRange)
    {
      if(_helper.IsNullOrEmpty(categoryId))
        throw new  ArgumentException("Invalid id");
      
      if (!await _categoriesRepository.ExistById(categoryId))
        throw new  NullReferenceException("Category doesn't exist");
      
      return  await _postsRepository.GetPostsByCategory(categoryId, maxRange);
    }
  }
}