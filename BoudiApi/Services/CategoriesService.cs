using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Helpers;
using BoudiApi.Models;
using BoudiApi.Repositories;

namespace BoudiApi.Services
{
  public class CategoriesService : ICategoriesService
  {
    private readonly ICategoriesRepository _categoriesRepository;
    private readonly IHelper _helper;
    public CategoriesService(ICategoriesRepository categoriesRepository, IHelper helper)
    {
      _categoriesRepository = categoriesRepository;
      _helper = helper;
    }
    
    public async Task<List<Category>> GetAll(string searchText, int maxRange)
    {
      return await _categoriesRepository.GetAll(searchText, maxRange);
    }
    
    public async Task<Category> GetSingle(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
      {
        throw new  ArgumentException("Invalid id");
      }
      
      if(! await _categoriesRepository.ExistById(id))
        throw new NullReferenceException("Category doesn't exist");
      
      return await _categoriesRepository.GetSingle(id);
    }
    
    public async Task<Category> CreateAsync(CrudCategoryDto categoryToAdd)
    {
      var slug = _helper.GenerateSlug(categoryToAdd.Name);
      
      if (await _categoriesRepository.ExistByName(categoryToAdd.Name) || await _categoriesRepository.ExistBySlug(slug))
        throw new InvalidDataException($"A Category with the same name or slug already exist");

      
      var modelDb =await _categoriesRepository.CreateAsync(categoryToAdd, slug);
      return modelDb;
    }
    
    public async Task<Category> UpdateAsync(Guid id, CrudCategoryDto categoryToEdit)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if (!await _categoriesRepository.ExistById(id))
        throw new  NullReferenceException("Category doesn't exist");
      
      var slug = _helper.GenerateSlug(categoryToEdit.Name);


      if (await _categoriesRepository.ExistByName(categoryToEdit.Name) || await _categoriesRepository.ExistBySlug(slug))
      {
        var similarCategory = await _categoriesRepository.GetByName(categoryToEdit.Name);
        
        if(similarCategory.Id != id)
          throw new InvalidDataException($"A Category already exist with the same name or slug");
        
        throw new InvalidDataException($"Im not going to call an update on a unchanged category");
      }
      
      var modelDb = await _categoriesRepository.UpdateAsync(id, categoryToEdit, slug);
      
      return modelDb;
    }
    
    public async Task<int> Delete(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if (!await _categoriesRepository.ExistById(id))
        throw new  NullReferenceException("Category doesn't exist");

      return await _categoriesRepository.Delete(id);
    }

  }
}