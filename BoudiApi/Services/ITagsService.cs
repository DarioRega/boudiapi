using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Services
{
  public interface ITagsService
  {
    Task<List<Tag>> GetAll(string searchText, int maxRange);
    Task<Tag> GetSingle(Guid id);
    Task<Tag> GetPostsBySingleTag(Guid tagId, int maxRange);
    Task<Tag> CreateAsync(CrudTagDto tagToAdd);
    Task<Tag> UpdateAsync(Guid id, CrudTagDto tagToUpdate);
    Task<int> Delete(Guid id);

  }
}