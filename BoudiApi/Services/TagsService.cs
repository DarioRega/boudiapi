using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Helpers;
using BoudiApi.Models;
using BoudiApi.Repositories;

namespace BoudiApi.Services
{
  public class TagsService : ITagsService
  {
    private readonly ITagsRepository _tagsRepository;
    private readonly IHelper _helper;
    
    public TagsService(ITagsRepository tagsRepository, IHelper helper)
    {
      _tagsRepository = tagsRepository;
      _helper = helper;
    }
    
    public async Task<List<Tag>> GetAll(string searchText, int maxRange)
    {
      return await _tagsRepository.GetAll(searchText, maxRange);
    }
    
    public async Task<Tag> GetSingle(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
      {
        throw new  ArgumentException("Invalid id");
      }
      
      if(!await _tagsRepository.ExistById(id))
        throw new NullReferenceException("Tag doesn't exist");
      
      return await _tagsRepository.GetSingle(id);
    }
    
    public async Task<Tag> CreateAsync(CrudTagDto categoryToAdd)
    {
      if(await _tagsRepository.ExistByName(categoryToAdd.Name))
        throw new InvalidDataException($"Tag already exist");

      var slug = _helper.GenerateSlug(categoryToAdd.Name);
      
      var modelDb =await _tagsRepository.CreateAsync(categoryToAdd, slug);
      return modelDb;
    }
    
    public async Task<Tag> UpdateAsync(Guid id, CrudTagDto tagToEdit)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if (!await _tagsRepository.ExistById(id))
        throw new  NullReferenceException("Tag doesn't exist");
      
      var slug = _helper.GenerateSlug(tagToEdit.Name);


      if (await _tagsRepository.ExistByName(tagToEdit.Name) || await _tagsRepository.ExistBySlug(slug))
      {
        var similarTag = await _tagsRepository.GetByName(tagToEdit.Name);
        
        if(similarTag.Id != id)
          throw new InvalidDataException($"A Tag already exist with the same name or slug");
        
        throw new InvalidDataException($"Im not going to call an update on a unchanged tag");
      }
      
      var modelDb = await _tagsRepository.UpdateAsync(id, tagToEdit, slug);
      
      return modelDb;
    }
    
    public async Task<int> Delete(Guid id)
    {
      if(_helper.IsNullOrEmpty(id))
        throw new  ArgumentException("Invalid id");
      
      if (!await _tagsRepository.ExistById(id))
        throw new  NullReferenceException("Tag doesn't exist");

      return await _tagsRepository.Delete(id);
    }
    
    public async Task<Tag> GetPostsBySingleTag(Guid tagId, int maxRange)
    {
      if(_helper.IsNullOrEmpty(tagId))
        throw new  ArgumentException("Invalid id");
      
      if (!await _tagsRepository.ExistById(tagId))
        throw new  NullReferenceException("Tag doesn't exist");
      
      return  await _tagsRepository.GetPostsBySingleTag(tagId, maxRange);
    }

  }
}