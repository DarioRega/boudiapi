using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Services
{
  public interface IUsersService
  {
    Task<List<User>> GetAll(int maxRange);
    Task<User> GetSingle(Guid id);
    Task<List<User>> GetUserFollowers(Guid id);
    Task<User> CreateAsync(CrudUserDto userToAdd);
    Task<User> UpdateAsync(Guid id, CrudUserDto userToEdit);
    Task<int> Delete(Guid id);
    Task<int> FollowUser(Guid id, Guid userId);
    Task<int> UnFollowUser(Guid id, Guid userId);
    Task<int> SavePost(Guid id, Guid postId);
    Task<int> UnSavePost(Guid id, Guid postId);
    Task<int> LikePost(Guid id, Guid postId);
    Task<int> UnLikePost(Guid id, Guid postId);
    Task<User> Login(LoginDto loginDto);


  }
}