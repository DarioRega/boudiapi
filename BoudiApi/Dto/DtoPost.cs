using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BoudiApi.Models;
using Microsoft.AspNetCore.Http;

namespace BoudiApi.Dto
{
  public class PostDto 
  {
    public Guid Id { get; set; }
    public string Title { get; set; }
    [Required]
    public string Slug { get; set; }
    [Required]
    public string Summary { get; set; }
    [Required]
    public string Body { get; set; }
    [Required]
    public Guid CategoryId { get; set; }
    public CategoryMiniatureDto Category { get; set; }
    public string ImageUrl { get; set; }
    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
    [Required]
    public Guid UserId { get; set; }
    public UserMiniatureDto User { get; set; }
    public virtual ICollection<CommentMiniatureDto> Comments { get; set; }
    public virtual ICollection<TagMiniatureDto> Tags { get; set; }
    public virtual ICollection<UserMiniatureDto> LikedBy { get; set; }
  }
  
  public class PostMiniatureDto 
  {
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Slug { get; set; }
    public string Summary { get; set; }
    public string Body { get; set; }
    public Guid CategoryId { get; set; }
    public CategoryMiniatureDto Category { get; set; }
    public string ImageUrl { get; set; }
    public Guid UserId { get; set; }
    public UserMiniatureDto User { get; set; }
    public int LikeCount { get; set; }
    public virtual ICollection<TagMiniatureDto> Tags { get; set; }
    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
  }
  public class PostUserDto 
  {
    public Guid Id { get; set; }
    public string Title { get; set; }
    [Required]
    public string Slug { get; set; }
    [Required]
    public string Summary { get; set; }
    [Required]
    public string Body { get; set; }
    [Required]
    public Guid CategoryId { get; set; }
    public CategoryMiniatureDto Category { get; set; }
    public virtual ICollection<TagMiniatureDto> Tags { get; set; }
    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
  }
  public class CrudPostDto 
  {
    [Required]
    public string Title { get; set; }
    [Required]
    public string Summary { get; set; }
    [Required]
    public string ImageUrl { get; set; }
    [Required]
    public string Body { get; set; }
    [Required]
    public Guid CategoryId { get; set; }
    [Required]
    public Guid UserId { get; set; }
    public ICollection<Guid> TagsId  { get; set; }
  }
  
  public class RelatedPostCategoryDto 
  {
    public Guid Id { get; set; }
    public ICollection<TagMiniatureDto> Tags  { get; set; }
    public UserMiniatureDto User { get; set; }
    public int LikeCount { get; set; }
    public ICollection<CommentMiniatureDto> Comments  { get; set; }
    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
  }

  public class RelatedPostTagDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Slug { get; set; }
    public ICollection<PostMiniatureDto> Posts  { get; set; }
    public DateTime? UpdatedAt { get; set; }
    public DateTime? CreatedAt { get; set; }

  }
}