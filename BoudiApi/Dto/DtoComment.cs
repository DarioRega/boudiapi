using System;
using System.ComponentModel.DataAnnotations;

namespace BoudiApi.Dto
{
  public class CommentDto
  {
    [Key]
    public Guid Id { get; set; }
    public Guid PostId { get; set; }
    public PostMiniatureDto Post { get; set; }
    [Required]
    public string Content { get; set; }
    public UserMiniatureDto User { get; set; }
    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
  }
  public class CrudCommentDto
  {
    [Required]
    public Guid PostId { get; set; }
    [Required]
    public Guid UserId { get; set; }
    [Required]
    public string Content { get; set; }
  }
  public class CommentMiniatureDto
  {
    [Key]
    public Guid Id { get; set; }
    public Guid PostId { get; set; }
    public string Content { get; set; }
    public Guid UserId { get; set; }
    public UserMiniatureDto User { get; set; }
    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
  }  
  public class CommentCreatedUpdatedDto
  {
    [Key]
    public Guid Id { get; set; }
    public Guid PostId { get; set; }
    public string Content { get; set; }
    public Guid UserId { get; set; }
    public DateTime? CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
  }
  
  
  
}