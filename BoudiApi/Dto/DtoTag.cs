using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BoudiApi.Dto
{
  public class TagDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Slug { get; set; }
    public virtual ICollection<RelatedPostTagDto> Posts { get; set; }
  }
    
    public class TagMiniatureDto
    {
      public Guid Id { get; set; }
      public string Name { get; set; }
      public string Slug { get; set; }
    }
    public class TagMiniatureWithCountDto
    {
      public Guid Id { get; set; }
      public string Name { get; set; }
      public string Slug { get; set; }
      public int PostCount { get; set; }
    }

    public class CrudTagDto
    {
      [Required]
      public string Name { get; set; }
    }
}