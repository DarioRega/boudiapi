using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BoudiApi.Dto
{
  public class CategoryDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Slug { get; set; }
    public string ThemeColor { get; set; }
    public virtual ICollection<RelatedPostCategoryDto> Posts { get; set; }
  }
    
    public class CategoryMiniatureDto
    {
      public Guid Id { get; set; }
      public string Name { get; set; }
      public string Slug { get; set; }
      public string ThemeColor { get; set; }
      public int PostCount { get; set; }
    }
    
    public class CrudCategoryDto
    {
      [Required]
      public string Name { get; set; }
      public string ThemeColor { get; set; }
    }
}