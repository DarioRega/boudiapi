using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BoudiApi.Models;

namespace BoudiApi.Dto
{
    public class UserMiniatureDto
    {
      public Guid Id { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string Avatar { get; set; }
      public string Description { get; set; }
      public int FollowingCount { get; set; }
      public int FollowersCount { get; set; }
      public int PostsWrittenCount { get; set; }
      public DateTime? CreatedAt { get; set; }
      public DateTime? UpdatedAt { get; set; }
    }
    public class UserMiniatureDtoWithPosts
    {
      public Guid Id { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string Avatar { get; set; }
      public string Description { get; set; }
      public int FollowingCount { get; set; }
      public int FollowersCount { get; set; }
      public int PostsWrittenCount { get; set; }
      public ICollection<PostMiniatureDto> Posts { get; set; }
      public DateTime? CreatedAt { get; set; }
      public DateTime? UpdatedAt { get; set; }
    }
  
    public class CrudUserDto
    {
      [Required]
      public string FirstName { get; set; }
      [Required]
      public string LastName { get; set; }
      [Required]
      public string Password { get; set; }
      [Required]
      public string Email { get; set; }
      [Required]
      public string Description { get; set; }
      [Required]
      public string Avatar { get; set; }
    }
    public class UserDto
    {
      public Guid Id { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string Password { get; set; }
      public string Email { get; set; }
      public string Description { get; set; }
      public string Avatar { get; set; }
      public virtual ICollection<PostMiniatureDto> Posts { get; set; }
      public virtual ICollection<PostMiniatureDto> LikedPosts { get; set; }
      public virtual ICollection<PostMiniatureDto> SavedPosts { get; set; }
      public virtual ICollection<UserMiniatureDto> Followers { get; set; }
      public virtual ICollection<UserMiniatureDto> Following { get; set; }
      public virtual ICollection<CommentMiniatureDto> Comments { get; set; }
      public DateTime? CreatedAt { get; set; }
      public DateTime? UpdatedAt { get; set; }
    }

    public class LoginDto
    {
      public string Email { get; set; }
      public string Password { get; set; }
    }
}