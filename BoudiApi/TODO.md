## Post
* ~~Get posts by categories~~
* ~~Get posts containing tag~~
* ~~Search implement~~

## Commment
* ~~CRUD~~
* ~~Check if delete comment, in post list its gone~~
* ~~Check if delete post, if comment deleted~~


## Test
* Pick few functions to test


## Front
* Prepare new models in jira ticket so Noé can start
* Expose to noe which endpoints to call and when.
* Prepare promises and an example for adaptation

## In case of more time
* Implement PostSeen (max 15 if easy) in User with many to many with SeenBy in Post, in post Dto SeenBy automapper the count of seen not user.