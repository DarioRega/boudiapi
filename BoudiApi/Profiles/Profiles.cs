using AutoMapper;
using BoudiApi.Dto;
using BoudiApi.Models;

namespace BoudiApi.Profiles
{
  public class Profiles : Profile
  {
    public Profiles()
    {
      
      // USERS
      CreateMap<User, UserMiniatureDto>()
        .ForMember(dest =>
          dest.Id, opt => opt.MapFrom(src => src.Id))
        .ForMember(dest =>
          dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
        .ForMember(dest =>
          dest.Description, opt => opt.MapFrom(src => src.Description))
        .ForMember(dest =>
          dest.LastName, opt => opt.MapFrom(src => src.LastName))
        .ForMember(dest =>
          dest.FollowersCount, opt => opt.MapFrom(src => src.Followers.Count))
        .ForMember(dest =>
          dest.FollowingCount, opt => opt.MapFrom(src => src.Following.Count))
        .ForMember(dest =>
          dest.PostsWrittenCount, opt => opt.MapFrom(src => src.Posts.Count))
        .ForMember(dest =>
          dest.Avatar, opt => opt.MapFrom(src => src.Avatar));

      CreateMap<User, UserMiniatureDtoWithPosts>()
        .ForMember(dest =>
          dest.FollowersCount, opt => opt.MapFrom(src => src.Followers.Count))
        .ForMember(dest =>
          dest.FollowingCount, opt => opt.MapFrom(src => src.Following.Count))
        .ForMember(dest =>
          dest.PostsWrittenCount, opt => opt.MapFrom(src => src.Posts.Count));

      
      CreateMap<User, UserDto>().ReverseMap();
      
      // POSTS
      CreateMap<Post, PostDto>()
        .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));

      CreateMap<Post, PostUserDto>().ReverseMap();
      
      CreateMap<Post, PostMiniatureDto>()
        .ForMember(dest => dest.LikeCount, opt => opt.MapFrom(src => src.LikedBy.Count));
     
      CreateMap<Post, RelatedPostCategoryDto>()
        .ForMember(dest => dest.LikeCount, opt => opt.MapFrom(src => src.LikedBy.Count));

      CreateMap<Post, RelatedPostTagDto>();

      // CATEGORIES
      CreateMap<Category, CategoryDto>();
      
      CreateMap<Category, CategoryMiniatureDto>()
        .ForMember(dest => dest.PostCount, opt => opt.MapFrom(src => src.Posts.Count));
      
      // TAGS
      CreateMap<Tag, TagDto>().ReverseMap();

      CreateMap<Tag, TagMiniatureDto>();
      CreateMap<Tag, TagMiniatureWithCountDto>()
        .ForMember(dest => dest.PostCount, opt => opt.MapFrom(src => src.Posts.Count));

      CreateMap<Tag, RelatedPostTagDto>();
      
      // COMMENTS
      CreateMap<Comment, CommentDto>().ReverseMap();
      
      CreateMap<Comment, CommentMiniatureDto>();
      
      CreateMap<Comment, CommentCreatedUpdatedDto>();
      

    }    
  }
}