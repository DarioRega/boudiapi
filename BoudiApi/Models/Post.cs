using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using BoudiApi.Data;

namespace BoudiApi.Models
{
  public class Post : BaseEntity
  {
    [Key]
    public Guid Id { get; set; }
    [Required]
    public Guid UserId { get; set; }
    public User User { get; set; }
    public string Title { get; set; }
    [Required]
    public string Slug { get; set; }
    [Required]
    public string Summary { get; set; }
    [Required]
    public string Body { get; set; }
    [Required]
    public Guid CategoryId { get; set; }
    public Category Category { get; set; }
    public string ImageUrl { get; set; }
    public virtual ICollection<Comment> Comments { get; set; }
    public virtual ICollection<Tag> Tags { get; set; }
    public virtual ICollection<User> LikedBy { get; set; }
    public virtual ICollection<User> SavedBy { get; set; }

    public Post()
    {
      this.Comments = new List<Comment>();
      this.Tags = new List<Tag>();
      this.LikedBy = new List<User>();
      this.SavedBy = new List<User>();
    }
  }
}