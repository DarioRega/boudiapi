using System;
using System.ComponentModel.DataAnnotations;

namespace BoudiApi.Models
{
  public class Comment : BaseEntity
  {
    [Key]
    public Guid Id { get; set; }
    [Required]
    public Guid PostId { get; set; }
    public Post Post { get; set; }
    [Required]
    public string Content { get; set; }
    [Required]
    public Guid UserId { get; set; }
    public User User { get; set; }
  }
}