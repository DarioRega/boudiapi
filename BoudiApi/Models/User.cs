using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BoudiApi.Models
{
  public class User : BaseEntity
  {
    [Key]
    public Guid Id { get; set; }
    [Required]
    public string Password { get; set; }
    [Required]
    public string FirstName { get; set; }
    [Required]
    public string LastName { get; set; }
    [Required]
    public string Email { get; set; }
    public string Description { get; set; }
    public string Avatar { get; set; }
    public ICollection<Post> Posts { get; set; }
    public ICollection<Comment> Comments { get; set; }
    public ICollection<Post> LikedPosts { get; set; }
    public ICollection<Post> SavedPosts { get; set; }
    public ICollection<User> Followers { get; set; }
    public ICollection<User> Following { get; set; }
    
    public User()
    {
      this.Posts = new List<Post>();
      this.LikedPosts = new List<Post>();
      this.SavedPosts = new List<Post>();
      this.Followers = new List<User>();
      this.Following = new List<User>();
      this.Comments = new List<Comment>();
    }
  }
 
}