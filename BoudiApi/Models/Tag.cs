using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BoudiApi.Models
{
  public class Tag : BaseEntity
  {
    [Key]
    public Guid Id { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public string Slug { get; set; }
    public virtual ICollection<Post> Posts { get; set; }
    
    public Tag()
    {
      this.Posts = new List<Post>();
    }
  }
}