using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BoudiApi.Models
{
  public class Category : BaseEntity
  {
    [Key]
    public Guid Id { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public string Slug { get; set; }
    [Required]
    public string ThemeColor { get; set; }
    public virtual ICollection<Post> Posts { get; set; }
    
    public Category()
    {
      this.Posts = new List<Post>();
    }
  }
}