using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BoudiApi.Dto;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace BoudiApi.Helpers
{
  public class Helper : IHelper
  {
    public readonly IWebHostEnvironment _hostEnvironment;
    public readonly IHttpContextAccessor _httpContextAccessor;

    public Helper(IWebHostEnvironment hostEnvironment, IHttpContextAccessor httpContextAccessor)
    {
      _hostEnvironment = hostEnvironment;
      _httpContextAccessor = httpContextAccessor;

    }

   public bool IsNullOrEmpty(Guid? guid)
    {
      if (guid.HasValue)
        if (guid == default(Guid))
          return true;
      return false;
    }

    public string GenerateSlug(string phrase)
    {
      var s = RemoveAccent(phrase).ToLower();
      s = Regex.Replace(s, @"[^a-z0-9\s-]", "");                      // remove invalid characters
      s = Regex.Replace(s, @"\s+", " ").Trim();                       // single space
      s = s.Substring(0, s.Length <= 45 ? s.Length : 45).Trim();      // cut and trim
      s = Regex.Replace(s, @"\s", "-");                               // insert hyphens
      return s.ToLower();
    }
    public string RemoveAccent(string txt) 
    { 
      byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt); 
      return System.Text.Encoding.ASCII.GetString(bytes); 
    }
    
    public async Task<string> UploadFileAndGetFileName(UploadContext model)  
    {  
      string uniqueFileName = "ErrorWhileUploading";  
  
      if (model.ImageFile != null)  
      {  
        uniqueFileName = GenerateUniqueFileName(model.ImageFile);
        
        // TODO SHOULD ADD TEST
        if (await UploadFile(model, uniqueFileName) != 1)
          return $"default{model.ContextTarget}Miniature.jpg";
      }  
      return uniqueFileName;  
    }

    public string GenerateUniqueFileName(IFormFile file)
    {
      return  Guid.NewGuid().ToString() + "_" + file.FileName;  
    }


    public async Task<int> UploadFile(UploadContext model, string uniqueFileName)
    {
      string uploadsFolder = Path.Combine(_hostEnvironment.WebRootPath, "images");
      string filePath = Path.Combine(uploadsFolder, uniqueFileName);

      var fileStream = new FileStream(filePath, FileMode.Create);
      try
      {
        await model.ImageFile.CopyToAsync(fileStream);
        return 1;
      }
      catch (Exception)
      {
        return 0;
      }
    }

    public string GetCurrentHostContextUrl()
    {
      var current = _httpContextAccessor.HttpContext;
      return $"{current.Request.Scheme}://{current.Request.Host}{current.Request.PathBase}";
    
    }
  }
}