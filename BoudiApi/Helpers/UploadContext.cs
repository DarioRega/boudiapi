using Microsoft.AspNetCore.Http;

namespace BoudiApi.Dto
{
    public class UploadContext
    {
      public ContextUpload ContextTarget;
      public IFormFile ImageFile;
      public string Name;
    }
    public enum ContextUpload
    {
      Post,
      User
    }
}