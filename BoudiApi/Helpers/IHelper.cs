using System;
using System.Threading.Tasks;
using BoudiApi.Dto;
using Microsoft.AspNetCore.Http;

namespace BoudiApi.Helpers
{
  public interface IHelper
  {
    bool IsNullOrEmpty(Guid? id);
    string GenerateSlug(string phrase);
    string RemoveAccent(string phrase);
    string GetCurrentHostContextUrl();
    string GenerateUniqueFileName(IFormFile file);
    Task<string> UploadFileAndGetFileName(UploadContext model);
    Task<int> UploadFile(UploadContext model, string generatedFileName);
  }
}