using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Helpers;
using BoudiApi.Models;
using BoudiApi.Repositories;
using BoudiApi.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;

namespace BoudiApi.Test.Services
{
  [TestFixture]
  public class PostsServiceTest
  {
    private Mock<IPostsRepository> MockPostsRepository { get; set; }
    private Mock<ICategoriesRepository> MockCategoriesRepository { get; set; }
    private Mock<IUsersRepository> MockUsersRepository { get; set; }
    private Mock<IHelper> MockHelperService { get; set; }
    private IHelper helper { get; set; }
    private Mock<IWebHostEnvironment> MockHostEnvironment { get; set; }
    private Mock<IHttpContextAccessor> MockHttpContextAccessor { get; set; }
    private IPostsService Sut { get; set; }
    private const string TestPrefix = "POSTSERVICE - ";

    [SetUp]
    public void Setup()
    {
      var mockRepository = new MockRepository(MockBehavior.Default);

      this.MockPostsRepository = mockRepository.Create<IPostsRepository>();
      this.MockCategoriesRepository = mockRepository.Create<ICategoriesRepository>();
      this.MockUsersRepository = mockRepository.Create<IUsersRepository>();
      this.MockHostEnvironment = mockRepository.Create<IWebHostEnvironment>();
      this.MockHttpContextAccessor = mockRepository.Create<IHttpContextAccessor>();
      this.MockHelperService = mockRepository.Create<IHelper>();

      // POST REPO SETUP
      this.MockPostsRepository.Setup(aPost => aPost.CreateAsync(It.IsAny<CrudPostDto>(), It.IsAny<string>()))
        .Returns(Task.FromResult(new Post()));

      this.MockPostsRepository
        .Setup(aPost => aPost.UpdateAsync(It.IsAny<Guid>(), It.IsAny<CrudPostDto>(), It.IsAny<string>()))
        .Returns(Task.FromResult(new Post()));
      
      this.MockPostsRepository.Setup(aPost => aPost.GetSingle(It.IsAny<Guid>()))
        .ReturnsAsync(new Post()); 
            
      this.MockPostsRepository.Setup(aPost => aPost.GetPostsRelatedToTags(It.IsAny<List<Guid>>(), It.IsAny<int>()))
        .ReturnsAsync(new List<Post>(){new Post(), new Post()});  
      
      this.MockPostsRepository.Setup(aPost => aPost.GetBySlug(It.IsAny<string>()))
        .ReturnsAsync(new Post(){Id =Guid.NewGuid() });
      
      this.MockPostsRepository.Setup(aPost => aPost.ExistById(It.IsAny<Guid>()))
        .ReturnsAsync(true);
      
      // USER REPO SETUP
      this.MockUsersRepository.Setup(aUser => aUser.ExistById(It.IsAny<Guid>()))
        .ReturnsAsync(true);
      
      // HELPER SERVICE SETUP
      this.MockHelperService.Setup(aHelper => aHelper.IsNullOrEmpty(It.IsAny<Guid>())).Returns(false);
      this.MockHelperService.Setup(aHelper => aHelper.GenerateSlug(It.IsAny<string>())).Returns(It.IsAny<string>());
      this.MockPostsRepository.Setup(aPost => aPost.ExistBySlug(It.IsAny<string>())).ReturnsAsync(false);
      this.MockHelperService.SetupAllProperties();

      this.Sut = new PostsService(MockPostsRepository.Object, MockCategoriesRepository.Object,
        MockUsersRepository.Object, MockHelperService.Object);

    }

      /// **********************************************************************
      /// <summary>
      /// Assert that related we get a post when requesting it by PostsService
      /// </summary>
      [TestCase(TestName = TestPrefix + "Get single with id")]
      public async Task ShouldReturnAPostWithoutExceptionsThrowns()
      {
        //Act
        var guid = Guid.NewGuid();
        var post = await this.Sut.GetSingle(guid);
        
        //Assert
        this.MockPostsRepository.Verify(s => s.GetSingle(It.IsAny<Guid>()), Times.Exactly(1));
        Assert.That(post, Is.Not.Null);
      }
      
      /// **********************************************************************
      /// <summary>
      /// Assert that an exception is raised when an post doesn't exist by id
      /// </summary>
      [TestCase(TestName = TestPrefix + "Try to get a post not existing")]
      public void ShouldThrowNullReferenceExceptionPostNotExist()
      {
        this.MockPostsRepository.Setup(aPost => aPost.ExistById(It.IsAny<Guid>()))
          .ReturnsAsync(false);

        Assert.ThrowsAsync<NullReferenceException>(() => this.Sut.GetSingle(Guid.NewGuid()));
        this.MockPostsRepository.Verify(s => s.GetSingle(It.IsAny<Guid>()), Times.Exactly(0));
      }
      
      /// **********************************************************************
      /// <summary>
      /// Assert that an exception is raised when provided Guid is null or empty
      /// </summary>
      [TestCase(TestName = TestPrefix + "Try to get a post with invalid Guid")]
      public void ShouldThrowArgumentExceptionInvalidId()
      {
        this.MockHelperService.Setup(aHelper => aHelper.IsNullOrEmpty(It.IsAny<Guid>())).Returns(true);
        
        Assert.ThrowsAsync<ArgumentException>(() => this.Sut.GetSingle(new Guid()));
        this.MockPostsRepository.Verify(s => s.GetSingle(It.IsAny<Guid>()), Times.Exactly(0));
      }
      
      /// **********************************************************************
      /// <summary>
      /// Assert that an exception is raised when try to update to update title of post and generated slug is already existing
      /// </summary>
      [TestCase(TestName = TestPrefix + "Update a post with an existing slug")]
      public void ShouldThrowInvalidDataWhenSlugAlreadyExist()
      {
        this.MockPostsRepository.Setup(aPost => aPost.ExistBySlug(It.IsAny<string>())).ReturnsAsync(true);
        
        Assert.ThrowsAsync<InvalidDataException>(() => this.Sut.UpdateAsync(Guid.NewGuid(), new CrudPostDto(){UserId = Guid.NewGuid()}));
        this.MockPostsRepository.Verify(s => s.UpdateAsync(It.IsAny<Guid>(), It.IsAny<CrudPostDto>(),It.IsAny<string>()), Times.Exactly(0));
        this.MockHelperService.Verify(s => s.IsNullOrEmpty(It.IsAny<Guid>()), Times.Exactly(2));
        this.MockUsersRepository.Verify(s => s.ExistById(It.IsAny<Guid>()), Times.Exactly(1));
        this.MockPostsRepository.Verify(s => s.ExistById(It.IsAny<Guid>()), Times.Exactly(1));
      }
      
      /// **********************************************************************
      /// <summary>
      /// Assert that an exception is raised when userId provided for post creating doesn't exist
      /// </summary>
      [TestCase(TestName = TestPrefix + "Try to create post with not existing user")]
      public void ShouldThrowExceptionWhenCreatingPostWithNotExistingUser()
      {
        this.MockUsersRepository.Setup(aUser => aUser.ExistById(It.IsAny<Guid>())).ReturnsAsync(false);
        
        Assert.ThrowsAsync<NullReferenceException>(() => this.Sut.CreateAsync(new CrudPostDto{Title = "test", UserId = Guid.NewGuid()}));
        this.MockUsersRepository.Verify(s => s.ExistById(It.IsAny<Guid>()), Times.Exactly(1));
        this.MockPostsRepository.Verify(s => s.ExistBySlug(It.IsAny<string>()), Times.Exactly(0));
        this.MockPostsRepository.Verify(s => s.CreateAsync(It.IsAny<CrudPostDto>(), It.IsAny<string>()), Times.Exactly(0));
      }
      
      /// **********************************************************************
      /// <summary>
      /// Assert that we got posts when providing tagsIdList
      /// </summary>
      [TestCase(TestName = TestPrefix + "Get posts related to tags")]
      public async Task ShouldReturnAListOfPostRelatedToTagList()
      {
        // Arrange
        var tagList = new List<Guid>(){Guid.NewGuid(),Guid.NewGuid()};
        // //Act
        var posts = await this.Sut.GetPostsRelatedToTags(tagList, 32);
        
        //Assert
        Assert.That(posts.Count, Is.EqualTo(2));
        this.MockPostsRepository.Verify(s => s.GetPostsRelatedToTags(It.IsAny<List<Guid>>(), It.IsAny<int>()), Times.Exactly(1));
      }
      
      /// **********************************************************************
      /// <summary>
      /// Assert that we got posts when providing tagsIdList
      /// </summary>
      [TestCase(TestName = TestPrefix + "Try to get posts related to tags with empty tag list id")]
      public void ShouldThrowExceptionWhenTagListIsEmpty()
      {
        //Assert
        Assert.ThrowsAsync<ArgumentException>(() => this.Sut.GetPostsRelatedToTags(It.IsAny<List<Guid>>(), It.IsAny<int>()));
        this.MockPostsRepository.Verify(s => s.GetPostsRelatedToTags(It.IsAny<List<Guid>>(), It.IsAny<int>()), Times.Exactly(0));
      }
      
      /// **********************************************************************
      /// <summary>
      /// Assert that post is deleted successfully
      /// </summary>
      [TestCase(TestName = TestPrefix + "Delete a post")]
      public async Task ShouldDeleteAPost()
      {
        //Arrange
        this.MockPostsRepository.Setup(x => x.Delete(It.IsAny<Guid>())).ReturnsAsync(1);
        //Act
        var isDeleted = await this.Sut.Delete(It.IsAny<Guid>());
        //Assert
        Assert.That(isDeleted, Is.EqualTo(1));
        this.MockPostsRepository.Verify(s => s.Delete(It.IsAny<Guid>()), Times.Exactly(1));
        this.MockPostsRepository.Verify(s => s.ExistById(It.IsAny<Guid>()), Times.Exactly(1));
        this.MockHelperService.Verify(s => s.IsNullOrEmpty(It.IsAny<Guid>()), Times.Exactly(1));
      }
    }
  }
