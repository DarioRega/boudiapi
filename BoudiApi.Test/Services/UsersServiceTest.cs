using System;
using System.IO;
using System.Threading.Tasks;
using BoudiApi.Dto;
using BoudiApi.Helpers;
using BoudiApi.Models;
using BoudiApi.Repositories;
using BoudiApi.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;

namespace BoudiApi.Test.Services
{
  [TestFixture]
  public class UsersServiceTest
  {
    private Mock<IPostsRepository> MockPostsRepository { get; set; }
    private Mock<IUsersRepository> MockUsersRepository { get; set; }
    private Mock<IHelper> MockHelperService { get; set; }
    private IHelper helper { get; set; }
    private Mock<IWebHostEnvironment> MockHostEnvironment { get; set; }
    private Mock<IHttpContextAccessor> MockHttpContextAccessor { get; set; }
    private IUsersService Sut { get; set; }
    private const string TestPrefix = "USERSSERVICES - ";

    [SetUp]
    public void Setup()
    {
      var mockRepository = new MockRepository(MockBehavior.Default);

      this.MockPostsRepository = mockRepository.Create<IPostsRepository>();
      this.MockUsersRepository = mockRepository.Create<IUsersRepository>();
      this.MockHostEnvironment = mockRepository.Create<IWebHostEnvironment>();
      this.MockHttpContextAccessor = mockRepository.Create<IHttpContextAccessor>();
      this.MockHelperService = mockRepository.Create<IHelper>();

      // POST REPO SETUP
      this.MockPostsRepository.Setup(aPost => aPost.ExistById(It.IsAny<Guid>()))
        .ReturnsAsync(true);

      // USER REPO SETUP
      this.MockUsersRepository.Setup(aUser => aUser.ExistById(It.IsAny<Guid>()))
        .ReturnsAsync(true);
      this.MockUsersRepository.Setup(aUser => aUser.GetSingle(It.IsAny<Guid>()))
        .ReturnsAsync(new User());
      this.MockUsersRepository.Setup(aUser => aUser.CreateAsync(It.IsAny<CrudUserDto>()))
        .ReturnsAsync(new User(){FirstName = "Daft"});
      
      this.MockUsersRepository.Setup(aUser => aUser.UpdateAsync(It.IsAny<Guid>() ,It.IsAny<CrudUserDto>()))
        .ReturnsAsync(new User(){FirstName = "Robert"});
      
      this.MockUsersRepository.Setup(aUser => aUser.ExistByEmail(It.IsAny<string>()))
        .ReturnsAsync(true);

      this.MockUsersRepository.Setup(aUser => aUser.GetSingleByEmail(It.IsAny<string>()))
        .ReturnsAsync(new User() {Id = Guid.NewGuid()});

      // HELPER SERVICE SETUP
      this.MockHelperService.Setup(aHelper => aHelper.IsNullOrEmpty(It.IsAny<Guid>())).Returns(false);
      this.MockHelperService.SetupAllProperties();

      this.Sut = new UsersService(MockUsersRepository.Object, MockPostsRepository.Object, MockHelperService.Object);
    }

    /// **********************************************************************
    /// <summary>
    /// Assert that related we get an user when requesting it by UserService
    /// </summary>
    [TestCase(TestName = TestPrefix + "Get single with id")]
    public async Task ShouldReturnAnUserWithoutExceptionsThrowns()
    {
      //Act
      var guid = Guid.NewGuid();
      var user = await this.Sut.GetSingle(guid);

      //Assert
      this.MockUsersRepository.Verify(s => s.GetSingle(It.IsAny<Guid>()), Times.Exactly(1));
      Assert.That(user, Is.Not.Null);
    }

    /// **********************************************************************
    /// <summary>
    /// Assert that an exception is raised when an post doesn't exist by id
    /// </summary>
    [TestCase(TestName = TestPrefix + "Get a user not existing")]
    public void ShouldThrowNullReferenceExceptionUserNotExist()
    {
      this.MockUsersRepository.Setup(aUser => aUser.ExistById(It.IsAny<Guid>()))
        .ReturnsAsync(false);

      Assert.ThrowsAsync<NullReferenceException>(() => this.Sut.GetSingle(Guid.NewGuid()));
      this.MockUsersRepository.Verify(s => s.GetSingle(It.IsAny<Guid>()), Times.Exactly(0));
    }

    /// **********************************************************************
    /// <summary>
    /// Assert that an exception is raised when provided Guid is null or empty
    /// </summary>
    [TestCase(TestName = TestPrefix + "Get a user with invalid Guid")]
    public void ShouldThrowArgumentExceptionInvalidId()
    {
      this.MockHelperService.Setup(aHelper => aHelper.IsNullOrEmpty(It.IsAny<Guid>())).Returns(true);

      Assert.ThrowsAsync<ArgumentException>(() => this.Sut.GetSingle(new Guid()));
      this.MockUsersRepository.Verify(s => s.GetSingle(It.IsAny<Guid>()), Times.Exactly(0));
    }
    
    /// **********************************************************************
    /// <summary>
    /// Assert that an exception is raised when provided Guid is null or empty
    /// </summary>
    [TestCase(TestName = TestPrefix + "Try to create user when email already exist")]
    public void ShouldThrowInvalidDataExceptionWhenCreateWithAnAlreadyExistingMail()
    {
      Assert.ThrowsAsync<InvalidDataException>(() => this.Sut.CreateAsync(new CrudUserDto(){Email = "test test"}));
      this.MockUsersRepository.Verify(s => s.CreateAsync(It.IsAny<CrudUserDto>()), Times.Exactly(0));
    }
    /// **********************************************************************
    /// <summary>
    /// Assert that user is done
    /// </summary>
    [TestCase(TestName = TestPrefix + "Try to create user when email already exist")]
    public async Task ShouldCreateUser()
    {    
      //Arrange
      this.MockUsersRepository.Setup(aUser => aUser.ExistByEmail(It.IsAny<string>()))
        .ReturnsAsync(false);
      
      var newUserDto = new CrudUserDto
      {
        FirstName = "Daft",
        LastName = "Mougi",
        Password = "Password1",
        Email = "email@email.com",
        Description = "Descri",
        Avatar = "http"
      };
      
      //Act
      var user = await this.Sut.CreateAsync(newUserDto);
      
      //Assert
      Assert.That(user,Is.Not.Null);
      Assert.That(user.FirstName,Is.EqualTo("Daft"));
      this.MockUsersRepository.Verify(s => s.CreateAsync(It.IsAny<CrudUserDto>()), Times.Exactly(1));
      this.MockUsersRepository.Verify(s => s.ExistByEmail(It.IsAny<string>()), Times.Exactly(1));
    }
    
    /// **********************************************************************
    /// <summary>
    /// Assert that an exception is raised when trying to update user mail but it already exist in db
    /// </summary>
    [TestCase(TestName = TestPrefix + "Try to update user mail when email already exist")]
    public void ShouldThrowExceptionWhenUpdatingUserWhenEmailAlreadyExistAndDifferentThanItsOwnId()
    {    
      Assert.ThrowsAsync<InvalidDataException>(() => this.Sut.UpdateAsync(Guid.NewGuid(), new CrudUserDto(){Email = "test test"}));

      this.MockHelperService.Verify(s => s.IsNullOrEmpty(It.IsAny<Guid>()), Times.Exactly(1));
      this.MockUsersRepository.Verify(s => s.ExistById(It.IsAny<Guid>()), Times.Exactly(1));
      this.MockUsersRepository.Verify(s => s.ExistByEmail(It.IsAny<string>()), Times.Exactly(1));
      this.MockUsersRepository.Verify(s => s.GetSingleByEmail(It.IsAny<string>()), Times.Exactly(1));
      this.MockUsersRepository.Verify(s => s.UpdateAsync(It.IsAny<Guid>(),It.IsAny<CrudUserDto>()), Times.Exactly(0));
    }
    
    /// **********************************************************************
    /// <summary>
    /// Assert that FirstName is updated 
    /// </summary>
    [TestCase(TestName = TestPrefix + "Create an user then Update it")]
    public async Task ShouldUpdateUserFirstName()
    {    
      //Arrange
      this.MockUsersRepository.Setup(aUser => aUser.ExistByEmail(It.IsAny<string>()))
        .ReturnsAsync(false);
      var newUserDto = new CrudUserDto
      {
        FirstName = "Daft",
        LastName = "Mougi",
        Password = "Password1",
        Email = "email@email.com",
        Description = "Descri",
        Avatar = "http"
      };
      
      //Act
      var user = await this.Sut.CreateAsync(newUserDto);
      var updatedUser = await this.Sut.UpdateAsync(user.Id, newUserDto);
      
      //Assert
      Assert.That(updatedUser.FirstName, Is.EqualTo("Robert"));
      this.MockHelperService.Verify(s => s.IsNullOrEmpty(It.IsAny<Guid>()), Times.Exactly(1));
      this.MockUsersRepository.Verify(s => s.ExistById(It.IsAny<Guid>()), Times.Exactly(1));
      this.MockUsersRepository.Verify(s => s.ExistByEmail(It.IsAny<string>()), Times.Exactly(2));
      this.MockUsersRepository.Verify(s => s.CreateAsync(It.IsAny<CrudUserDto>()), Times.Exactly(1));
      this.MockUsersRepository.Verify(s => s.GetSingleByEmail(It.IsAny<string>()), Times.Exactly(0));
      this.MockUsersRepository.Verify(s => s.UpdateAsync(It.IsAny<Guid>(),It.IsAny<CrudUserDto>()), Times.Exactly(1));
    }
  }
}
