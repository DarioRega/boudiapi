## Projet Dario Regazzoni

1. [Utilisation built in online](#utilisation-online)
2. [Endpoints online](#endpoints-online)
3. [Utilisation locale](#utilisation-locale)
4. [Endpoints locaux](#endpoints-locaux)
5. [Frontend install, si nécessaire](#si-vous-souhaitez-frontend-install)
6. [Diagramme activité](#diagramme-dactivite)
7. [Diagramme use case](#diagramme-duse-case)



## Utilisation online
J'ai déployé cette appli sur Azure, si vous voulez accéder au swagger voici l'url : 

Swagger : **[Bloodi Api Swagger](https://boudiapi20210207145925.azurewebsites.net/swagger/index.html)**  

BaseURL : **[Bloodi Api](https://bloodi-front.dariorega.com/posts)**  (404 si vous référencez pas un /posts ou ect..)

 
J'ai également déployé la SPA sur un shared hosting, si vous voulez vous simplifier la vie pour ajouter des données ou update, avec cette interface c'est pas mal : 
**[Bloodi Front](https://bloodi-front.dariorega.com)**  

Ps: concernant les données, j'éspère qu'il restera quelque chose car j'ai donné l'info à votre collègue Yassine, peut être qu'il aura déjà tout supprimer ;)

#### Endpoints online
* [/posts](https://boudiapi20210207145925.azurewebsites.net/posts)
* [/users](https://boudiapi20210207145925.azurewebsites.net/users)
* [/comments](https://boudiapi20210207145925.azurewebsites.net/comments)
* [/tags](https://boudiapi20210207145925.azurewebsites.net/tags)
* [/categories](https://boudiapi20210207145925.azurewebsites.net/categories)

---

## Utilisation locale


### Backend

````
git clone https://github.com/DarioRega/bloodi-api.git
````

```
cd bloodi-api
```

```
dotnet restore
```

```
dotnet test
```

```
cd BoudiApi
```

```
dotnet run
```

**Go to [Swagger](https://localhost:5001/swagger/index.html)**

---

#### Endpoints locaux
* [/posts](https://localhost:5001/posts)
* [/users](https://localhost:5001/users)
* [/comments](https://localhost:5001/comments)
* [/tags](https://localhost:5001/tags)
* [/categories](https://localhost:5001/categories)

---
### Si vous souhaitez, Frontend install
```
git clone https://github.com/DarioRega/noe-dario-miniprojet-epsic.git
```

```
cd bloodifront && npm install
```

```
ng serve
```

**Go to [http://localhost:4200](http://localhost:4200)**

---

### Diagramme d'activite

![Diagrame activité](https://i.imgur.com/4RIIIPu.jpg)

--- 
### Diagramme d'use case

![Diagrame use case](https://i.imgur.com/YhZi5wi.jpg)

---

**[Miro board](https://miro.com/app/board/o9J_lVAfc78=/)** pour voir de meilleure qualité et zoom possible des deux diagrammes
